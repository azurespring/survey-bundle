<?php
/*
 * PermanentIDGeneratorInterface.php
 */

namespace AzureSpring\Bundle\SurveyBundle\EventListener;

/**
 * PermanentIDGenerator
 */
interface PermanentIDGeneratorInterface
{
    /**
     * @return string
     */
    public function generate();
}
