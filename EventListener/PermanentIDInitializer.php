<?php
/*
 * PermanentIDInitializer.php
 */

namespace AzureSpring\Bundle\SurveyBundle\EventListener;

use AzureSpring\Bundle\SurveyBundle\Entity\Traits\PermanentIDTrait;
use Doctrine\Common\Annotations\Reader;
use Doctrine\Common\Persistence\Event\LifecycleEventArgs;

/**
 * PermanentIDInitializer
 */
class PermanentIDInitializer
{
    private $generator;

    private $reader;

    /**
     * Constructor.
     *
     * @param PermanentIDGeneratorInterface $generator
     * @param Reader                        $reader
     */
    public function __construct(PermanentIDGeneratorInterface $generator, Reader $reader)
    {
        $this->generator = $generator;
        $this->reader = $reader;
    }

    /**
     * @param LifecycleEventArgs $args
     *
     * @throws \ReflectionException
     */
    public function prePersist(LifecycleEventArgs $args)
    {
        $class = new \ReflectionClass($object = $args->getObject());
        while ($class) {
            if (!in_array(PermanentIDTrait::class, $class->getTraitNames())) {
                $class = $class->getParentClass();

                continue;
            }

            if (!$object->getPermanentID()) {
                $object->setPermanentID($this->generator->generate());
            }
            break;
        }
    }
}
