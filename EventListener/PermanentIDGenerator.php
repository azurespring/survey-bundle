<?php
/*
 * PermanentIDGenerator
 */

namespace AzureSpring\Bundle\SurveyBundle\EventListener;

use Ramsey\Uuid\Uuid;

/**
 * PermanentIDGenerator
 */
class PermanentIDGenerator implements PermanentIDGeneratorInterface
{
    /**
     * @inheritDoc
     *
     * @throws \Exception
     */
    public function generate()
    {
        $s = strtr(rtrim(base64_encode(Uuid::uuid1()->getBytes()), '='), '+/', '-_');

        return $s[0] !== '-' ? $s : $this->generate();
    }
}
