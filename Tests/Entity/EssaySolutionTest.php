<?php

namespace Entity;

use AzureSpring\Bundle\SurveyBundle\Entity\EssaySolution;
use Doctrine\Common\Annotations\AnnotationRegistry;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Validator\Validation;

class EssaySolutionTest extends TestCase
{
    /**
     * @test
     * @dataProvider validateProvider
     */
    public function validate(int $count, EssaySolution $solution)
    {
        AnnotationRegistry::registerLoader('class_exists');
        $validator = Validation::createValidatorBuilder()
            ->enableAnnotationMapping()
            ->getValidator();

        $validations = $validator->validate($solution);
        $this->assertCount($count, $validations);
    }

    public function validateProvider()
    {
        return [
            [1, new EssaySolution()],
            [0, (new EssaySolution())->setParagraph('Blah-blah-blah')],
        ];
    }
}
