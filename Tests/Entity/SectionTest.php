<?php

namespace AzureSpring\Bundle\SurveyBundle\Entity;

use PHPUnit\Framework\TestCase;

class SectionTest extends TestCase
{
    /**
     * @test
     */
    public function grabInterests()
    {
        $this->assertEquals([], (new Section())->grabInterests());
    }
}
