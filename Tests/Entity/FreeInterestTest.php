<?php

namespace AzureSpring\Bundle\SurveyBundle\Entity;

use Doctrine\Common\Annotations\AnnotationRegistry;
use JMS\Serializer\SerializerBuilder;
use PHPUnit\Framework\TestCase;

class FreeInterestTest extends TestCase
{
    /**
     * @test
     */
    public function serialize()
    {
        AnnotationRegistry::registerLoader('class_exists');
        $serializer = SerializerBuilder::create()->build();
        $freeInterest = (new FreeInterest())
            ->setPermanentID('9BC6D156-E1DA-4FFC-BF23-A8CE22887641')
            ->setLabel('Lorem ipsum dolor sit amet,')
        ;

        $this->assertJsonStringEqualsJsonString(
            '{"permanent_id":"9BC6D156-E1DA-4FFC-BF23-A8CE22887641","label":"Lorem ipsum dolor sit amet,","local_labels":[]}',
            $serializer->serialize($freeInterest, 'json')
        );
    }
}
