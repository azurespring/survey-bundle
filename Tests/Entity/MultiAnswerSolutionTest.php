<?php

namespace Entity;

use AzureSpring\Bundle\SurveyBundle\Entity\MultiAnswerSolution;
use AzureSpring\Bundle\SurveyBundle\Entity\Option;
use Doctrine\Common\Annotations\AnnotationRegistry;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Validator\Validation;

class MultiAnswerSolutionTest extends TestCase
{
    /**
     * @test
     * @dataProvider validateProvider
     */
    public function validate(int $count, MultiAnswerSolution $solution)
    {
        AnnotationRegistry::registerLoader('class_exists');
        $validator = Validation::createValidatorBuilder()
            ->enableAnnotationMapping()
            ->getValidator();

        $validations = $validator->validate($solution);
        $this->assertCount($count, $validations);
    }

    public function validateProvider()
    {
        return [
            [1, new MultiAnswerSolution()],
            [0, (new MultiAnswerSolution())->addOption(new Option())],
            [0, (new MultiAnswerSolution())->setParagraph('Blah-blah-blah')],
        ];
    }
}
