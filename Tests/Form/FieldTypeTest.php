<?php

namespace AzureSpring\Bundle\SurveyBundle\Form;

use AzureSpring\Bundle\SurveyBundle\Entity\ChoiceScalar;
use AzureSpring\Bundle\SurveyBundle\Entity\EssayScalar;
use AzureSpring\Bundle\SurveyBundle\Entity\FeatureInterest;
use AzureSpring\Bundle\SurveyBundle\Entity\Footnote;
use AzureSpring\Bundle\SurveyBundle\Entity\FreeInterest;
use AzureSpring\Bundle\SurveyBundle\Entity\Option;
use AzureSpring\Bundle\SurveyBundle\Entity\OptionGroup;
use AzureSpring\Bundle\SurveyBundle\Entity\Predicate;
use AzureSpring\Bundle\SurveyBundle\Entity\RatingVector;
use AzureSpring\Bundle\SurveyBundle\Entity\Section;
use PHPUnit\Framework\Assert;
use Symfony\Component\Form\Test\TypeTestCase;

class FieldTypeTest extends TypeTestCase
{
    /**
     * @test
     */
    public function submitOK()
    {
        $form = $this->factory->create(FieldType::class);

        $form->submit([
            'permanentID'  => '# 1',
            'serialNumber' => 1,
            'label'        => 'SECTION 1',
            'children'     => [
                [
                    'permanentID'  => '## 1',
                    'serialNumber' => 1,
                    'interest'     => [
                        'permanentID' => 'I1',
                        'label'       => 'Q1',
                    ],
                    'multiple'     => false,
                    'scratch'      => 'scratch',
                    'prompt'       => 'prompt',
                    'options'      => [
                        'permanentID'  => '1.1',
                        'serialNumber' => 1,
                        'label'        => 'G',
                        'children'     => [
                            [
                                'permanentID'  => '1.1.1',
                                'serialNumber' => 1,
                                'label'        => '1A',
                            ],
                            [
                                'permanentID'  => '1.1.2',
                                'serialNumber' => 2,
                                'label'        => '1B',
                            ],
                        ],
                    ],
                ],
                [
                    'permanentID'  => '## 2',
                    'serialNumber' => 2,
                    'label'        => 'Q2',
                    'interests'    => [
                        [
                            'permanentID' => 'I2',
                            'label'       => 'Q2.1',
                        ],
                        [
                            'permanentID' => 'I3',
                            'label'       => 'Q2.2',
                        ],
                    ],
                    'scale'        => 5,
                    'captions'     => [
                        '1'        => 'Lo',
                        '5'        => 'Hi',
                    ],
                ],
                [
                    'permanentID'  => '## 3',
                    'serialNumber' => 3,
                    'label'        => 'Q3',
                    'required'     => false,
                ],
                [
                    'permanentID'  => '## 4',
                    'serialNumber' => 4,
                    'label'        => 'Q4',
                    'predicate'    => '~',
                    'footnotes'    => [
                        'footnote',
                    ],
                ]
            ],
        ]);
        $this->assertTrue($form->isSynchronized());

        $field = (new Section())
            ->setPermanentID('# 1')
            ->setSerialNumber(1)
            ->setLabel('SECTION 1')
            ->setRequired(true)
            ->addChild(
                (new ChoiceScalar())
                ->setPermanentID('## 1')
                ->setSerialNumber(1)
                ->setInterest(
                    (new FreeInterest())
                    ->setPermanentID('I1')
                    ->setLabel('Q1')
                )
                ->setMultiple(false)
                ->setRequired(true)
                ->setScratch('scratch')
                ->setPrompt('prompt')
                ->setOptions(
                    (new OptionGroup())
                    ->setPermanentID('1.1')
                    ->setSerialNumber(1)
                    ->setLabel('G')
                    ->addChild(
                        (new Option())
                        ->setPermanentID('1.1.1')
                        ->setSerialNumber(1)
                        ->setLabel('1A')
                    )
                    ->addChild(
                        (new Option())
                        ->setPermanentID('1.1.2')
                        ->setSerialNumber(2)
                        ->setLabel('1B')
                    )
                )
            )
            ->addChild(
                (new RatingVector())
                ->setLabel('Q2')
                ->setPermanentID('## 2')
                ->setSerialNumber(2)
                ->addInterest(
                    (new FeatureInterest())
                    ->setPermanentID('I2')
                    ->setLabel('Q2.1')
                )
                ->addInterest(
                    (new FeatureInterest())
                    ->setPermanentID('I3')
                    ->setLabel('Q2.2')
                )
                ->setRequired(true)
                ->setScale(5)
                ->setCaptions([
                    1 => 'Lo',
                    5 => 'Hi',
                ])
            )
            ->addChild(
                (new EssayScalar())
                ->setPermanentID('## 3')
                ->setSerialNumber(3)
                ->setInterest(
                    (new FreeInterest())
                    ->setLabel('Q3')
                )
                ->setRequired(false)
            )
            ->addChild(
                (new Predicate())
                ->setPermanentID('## 4')
                ->setSerialNumber(4)
                ->setInterest(
                    (new FreeInterest())
                    ->setLabel('Q4')
                )
                ->setRequired(true)
                ->addFootnote(
                    (new Footnote())
                    ->setParagraph('footnote')
                )
            )
        ;
        /** @var Section $data */
        $data = $form->getData();
        $this->assertEquals($field, $data);
        $this->assertFalse($data->getChildren()[2]->isRequired());
    }
}
