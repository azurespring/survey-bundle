<?php

namespace AzureSpring\Bundle\SurveyBundle\Form;

use AzureSpring\Bundle\SurveyBundle\Entity\EssayScalar;
use AzureSpring\Bundle\SurveyBundle\Entity\FreeInterest;
use AzureSpring\Bundle\SurveyBundle\Entity\Section;
use AzureSpring\Bundle\SurveyBundle\Model\AbstractCase;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\Test\TypeTestCase;

class CaseTypeTest extends TypeTestCase
{
    /**
     * @test
     */
    public function create()
    {
        $this->assertInstanceOf(FormInterface::class, $this->factory->create(CaseType::class, null, ['section' => new Section()]));
    }

    /**
     * @test
     */
    public function submit()
    {
        $section = (new Section())
            ->setRequired(true)
            ->addChild(
                (new EssayScalar())
                ->setInterest((new FreeInterest())->setPermanentID('A796FF45-5C43-489B-B045-C9EB05506E13'))
                ->setRequired(true)
            )
            ->addChild(
                (new EssayScalar())
                ->setInterest((new FreeInterest())->setPermanentID('6363CF97-0EA4-4F7D-B6B8-C0AF393C54B6'))
            )
        ;

        $case = new class() extends AbstractCase {
        };
        $form = $this->factory->create(CaseType::class, $case, [
            'section' => $section,
        ]);

        $form->submit(null);
        $this->assertCount(1, $case->getSolutions());
    }
}
