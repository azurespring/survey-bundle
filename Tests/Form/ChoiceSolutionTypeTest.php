<?php

namespace AzureSpring\Bundle\SurveyBundle\Form;

use AzureSpring\Bundle\SurveyBundle\Entity\AbstractOption;
use AzureSpring\Bundle\SurveyBundle\Entity\ChoiceScalar;
use AzureSpring\Bundle\SurveyBundle\Entity\ChoiceSolution;
use AzureSpring\Bundle\SurveyBundle\Entity\EssaySolution;
use AzureSpring\Bundle\SurveyBundle\Entity\FreeInterest;
use AzureSpring\Bundle\SurveyBundle\Entity\MultiAnswerSolution;
use AzureSpring\Bundle\SurveyBundle\Entity\Option;
use AzureSpring\Bundle\SurveyBundle\Entity\OptionGroup;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\Common\Persistence\Mapping\ClassMetadata;
use Doctrine\ORM\EntityManagerInterface;
use PHPUnit\Framework\MockObject\MockObject;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\PreloadedExtension;
use Symfony\Component\Form\Test\TypeTestCase;

class ChoiceSolutionTypeTest extends TypeTestCase
{
    /**
     * @var ManagerRegistry|MockObject
     */
    private $registry;

    protected function setUp(): void
    {
        $m = $this->createMock(ClassMetadata::class);
        $m
            ->method('getIdentifierFieldNames')
            ->willReturn(['id'])
        ;

        $em = $this->createMock(EntityManagerInterface::class);
        $em
            ->method('getClassMetadata')
            ->with($this->callback(function ($subject) {
                return in_array($subject, [AbstractOption::class, Option::class]);
            }))
            ->willReturn($m)
        ;

        $this->registry = $this->createMock(ManagerRegistry::class);
        $this->registry
            ->method('getManagerForClass')
            ->with($this->callback(function ($subject) {
                return in_array($subject, [AbstractOption::class, Option::class]);
            }))
            ->willReturn($em)
        ;

        parent::setUp();
    }

    protected function getExtensions()
    {
        $type = new EntityType($this->registry);

        return [
            new PreloadedExtension([$type], [])
        ];
    }

    /**
     * @test
     */
    public function submitWithoutScratchOK()
    {
        $question = $this->makeQuestion();
        $form = $this->factory->create(ChoiceSolutionType::class, null, ['interest' => $question->getInterest()]);

        $form->submit(['option' => $question->getOptions()->rollOut()[0]->getPermanentID()]);
        $this->assertTrue($form->isSynchronized());

        $solution = (new ChoiceSolution())
            ->setInterest($question->getInterest())
            ->setOption($question->getOptions()->rollOut()[0])
        ;
        $data = $form->getData();
        $this->assertEquals($solution, $data);
    }

    /**
     * @test
     */
    public function submitWithScratchOK()
    {
        $question = $this->makeQuestion(false, 'Other');
        $form = $this->factory->create(ChoiceSolutionType::class, null, ['interest' => $question->getInterest()]);

        $form->submit(['option' => $question->getOptions()->rollOut()[0]->getPermanentID()]);
        $this->assertTrue($form->isSynchronized());

        $solution = (new ChoiceSolution())
            ->setInterest($question->getInterest())
            ->setOption($question->getOptions()->rollOut()[0])
        ;
        $data = $form->getData();
        $this->assertEquals($solution, $data);
    }

    /**
     * @test
     */
    public function submitScratchOK()
    {
        $question = $this->makeQuestion(false, 'Other');
        $form = $this->factory->create(ChoiceSolutionType::class, null, ['interest' => $question->getInterest()]);

        $form->submit(['paragraph' => 'paragraph']);
        $this->assertTrue($form->isSynchronized());

        $solution = (new EssaySolution())
            ->setInterest($question->getInterest())
            ->setParagraph('paragraph')
        ;
        $data = $form->getData();
        $this->assertEquals($solution, $data);
    }

    /**
     * @test
     */
    public function submitScratchToChoiceOK()
    {
        $question = $this->makeQuestion(false, 'Other');
        $form = $this->factory->create(ChoiceSolutionType::class, new ChoiceSolution(), ['interest' => $question->getInterest()]);

        $form->submit(['paragraph' => 'paragraph']);
        $this->assertTrue($form->isSynchronized());

        $solution = (new EssaySolution())
            ->setInterest($question->getInterest())
            ->setParagraph('paragraph')
        ;
        $data = $form->getData();
        $this->assertEquals($solution, $data);
    }

    /**
     * @test
     */
    public function submitMultiAnswerOK()
    {
        $question = $this->makeQuestion(true);
        $form = $this->factory->create(ChoiceSolutionType::class, null, ['interest' => $question->getInterest()]);

        $form->submit([
            'options' => [
                $question->getOptions()->getChildren()[0]->getPermanentID(),
                $question->getOptions()->getChildren()[1]->getPermanentID(),
            ],
        ]);
        $this->assertTrue($form->isSynchronized());

        $solution = (new MultiAnswerSolution())
            ->setInterest($question->getInterest())
            ->addOption($question->getOptions()->getChildren()[0])
            ->addOption($question->getOptions()->getChildren()[1])
        ;
        $data = $form->getData();
        $this->assertEquals($solution, $data);
    }

    /**
     * @test
     */
    public function submitMultiAnswerWithScratchOK()
    {
        $question = $this->makeQuestion(true, 'Other');
        $form = $this->factory->create(ChoiceSolutionType::class, null, ['interest' => $question->getInterest()]);

        $form->submit([
            'options' => [
                $question->getOptions()->getChildren()[0]->getPermanentID(),
                $question->getOptions()->getChildren()[1]->getPermanentID(),
            ],
            'paragraph' => 'p',
        ]);
        $this->assertTrue($form->isSynchronized());

        $solution = (new MultiAnswerSolution())
            ->setInterest($question->getInterest())
            ->addOption($question->getOptions()->getChildren()[0])
            ->addOption($question->getOptions()->getChildren()[1])
            ->setParagraph('p')
        ;
        $data = $form->getData();
        $this->assertEquals($solution, $data);
    }

    /**
     * @param bool        $multiple
     * @param string|null $scratch
     *
     * @return ChoiceScalar
     */
    private function makeQuestion(bool $multiple = false, ?string $scratch = null)
    {
        return (new ChoiceScalar())
            ->setInterest(
                (new FreeInterest())
                ->setLabel('A free interest')
            )
            ->setOptions(
                (new OptionGroup())
                ->addChild(
                    (new Option())
                    ->setPermanentID('# 1')
                    ->setLabel('An option')
                )
                ->addChild(
                    (new Option())
                    ->setPermanentID('# 2')
                    ->setLabel('Another option')
                )
            )
            ->setMultiple($multiple)
            ->setScratch($scratch)
        ;
    }
}
