<?php

namespace AzureSpring\Bundle\SurveyBundle\Form;

use AzureSpring\Bundle\SurveyBundle\Entity\Option;
use AzureSpring\Bundle\SurveyBundle\Entity\OptionGroup;
use Symfony\Component\Form\Test\TypeTestCase;

class OptionTypeTest extends TypeTestCase
{
    /**
     * @test
     */
    public function submitOK()
    {
        $form = $this->factory->create(OptionType::class, $optionGroup = new OptionGroup());

        $form->submit([
            'permanentID'  => '# 1',
            'serialNumber' => 1,
            'label'        => 'G',
            'children'     => [
                [
                    'permanentID'  => '## 1',
                    'serialNumber' => 1,
                    'label'        => 'A',
                ],
                [
                    'permanentID'  => '## 2',
                    'serialNumber' => 2,
                    'label'        => 'B',
                ],
            ]
        ]);
        $this->assertTrue($form->isSynchronized());

        $option = (new OptionGroup())
            ->setPermanentID('# 1')
            ->setSerialNumber(1)
            ->setLabel('G')
            ->addChild(
                (new Option())
                ->setPermanentID('## 1')
                ->setSerialNumber(1)
                ->setLabel('A')
            )
            ->addChild(
                (new Option())
                    ->setPermanentID('## 2')
                    ->setSerialNumber(2)
                    ->setLabel('B')
            )
        ;
        $data = $form->getData();
        $this->assertEquals($option, $data);
        $this->assertSame($optionGroup, $data);
    }
}
