<?php

namespace AzureSpring\Bundle\SurveyBundle\Form;

use AzureSpring\Bundle\SurveyBundle\Entity\EssayScalar;
use AzureSpring\Bundle\SurveyBundle\Entity\EssaySolution;
use AzureSpring\Bundle\SurveyBundle\Entity\FreeInterest;
use Symfony\Component\Form\Test\TypeTestCase;

class EssaySolutionTypeTest extends TypeTestCase
{
    /**
     * @test
     */
    public function submitOK()
    {
        $question = (new EssayScalar())
            ->setInterest(
                (new FreeInterest())
                ->setLabel('A free interest')
            )
        ;
        $form = $this->factory->create(EssaySolutionType::class, null, ['interest' => $question->getInterest()]);

        $form->submit(['paragraph' => 'paragraph']);
        $this->assertTrue($form->isSynchronized());

        $solution = (new EssaySolution())
            ->setInterest($question->getInterest())
            ->setParagraph('paragraph')
        ;
        $data = $form->getData();
        $this->assertEquals($solution, $data);
    }
}
