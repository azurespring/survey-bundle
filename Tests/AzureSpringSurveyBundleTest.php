<?php

namespace AzureSpring\Bundle\SurveyBundle;

use PHPUnit\Framework\TestCase;
use Symfony\Component\HttpKernel\Bundle\Bundle;

class AzureSpringSurveyBundleTest extends TestCase
{
    /**
     * @test
     */
    public function construct()
    {
        $bundle = new AzureSpringSurveyBundle();

        $this->assertInstanceOf(Bundle::class, $bundle);
    }
}
