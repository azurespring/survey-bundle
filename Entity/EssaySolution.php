<?php
/*
 * EssaySolution.php
 */

namespace AzureSpring\Bundle\SurveyBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity()
 *
 * @method EssayScalar getQuestion()
 */
class EssaySolution extends AbstractSolution
{
    /**
     * @ORM\Column(type="text", nullable=true)
     *
     * @Assert\NotBlank()
     */
    private $paragraph;

    /**
     * @return string|null
     */
    public function getParagraph(): ?string
    {
        return $this->paragraph;
    }

    /**
     * @param string|null $paragraph
     *
     * @return $this
     */
    public function setParagraph(?string $paragraph): self
    {
        $this->paragraph = $paragraph;

        return $this;
    }

    /**
     * @inheritDoc
     */
    public function distill()
    {
        return ['paragraph' => $this->getParagraph()];
    }
}
