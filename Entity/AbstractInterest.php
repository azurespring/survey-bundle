<?php
/*
 * AbstractInterest.php
 */

namespace AzureSpring\Bundle\SurveyBundle\Entity;

use AzureSpring\Bundle\SurveyBundle\Entity\Traits\LabelTrait;
use AzureSpring\Bundle\SurveyBundle\Entity\Traits\PermanentIDTrait;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;

/**
 * @ORM\Entity()
 * @ORM\InheritanceType("JOINED")
 */
abstract class AbstractInterest implements LabelInterface
{
    use PermanentIDTrait, LabelTrait;

    /**
     * @ORM\Id()
     * @ORM\Column(type="string", length=255)
     *
     * @Serializer\SerializedName("permanent_id")
     */
    protected $permanentID;

    /**
     * @ORM\OneToMany(targetEntity="AzureSpring\Bundle\SurveyBundle\Entity\InterestLabel", mappedBy="interest", orphanRemoval=true, cascade={"persist"})
     */
    private $localLabels;

    /**
     * Constructor.
     */
    public function __construct()
    {
        $this->localLabels = new ArrayCollection();
    }

    /**
     * @return AbstractQuestion
     */
    abstract public function getQuestion();

    /**
     * @return Collection|InterestLabel[]
     */
    public function getLocalLabels(): Collection
    {
        return $this->localLabels;
    }

    /**
     * @param InterestLabel $localLabel
     *
     * @return $this
     */
    public function addLocalLabel(InterestLabel $localLabel): self
    {
        if (!$this->localLabels->contains($localLabel)) {
            $this->localLabels[] = $localLabel;
            $localLabel->setInterest($this);
        }

        return $this;
    }

    /**
     * @param InterestLabel $localLabel
     *
     * @return $this
     */
    public function removeLocalLabel(InterestLabel $localLabel): self
    {
        if ($this->localLabels->contains($localLabel)) {
            $this->localLabels->removeElement($localLabel);
            // set the owning side to null (unless already changed)
            if ($localLabel->getInterest() === $this) {
                $localLabel->setInterest(null);
            }
        }

        return $this;
    }

    /**
     * @return InterestLabel
     */
    public function localizeLabel(): InterestLabel
    {
        $this->addLocalLabel($label = new InterestLabel());

        return $label;
    }

    /**
     * @inheritDoc
     */
    public function __toString()
    {
        return "\"{$this->getLabel()}\" <{$this->getPermanentID()}>";
    }
}
