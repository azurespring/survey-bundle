<?php
/*
 * ChoiceSolution.php
 */

namespace AzureSpring\Bundle\SurveyBundle\Entity;

use AzureSpring\Bundle\SurveyBundle\Form\Model\ChoiceSolutionInterface;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity()
 *
 * @method ChoiceScalar|ChoiceVector getQuestion()
 */
class ChoiceSolution extends AbstractSolution implements ChoiceSolutionInterface
{
    /**
     * @ORM\ManyToOne(targetEntity="AzureSpring\Bundle\SurveyBundle\Entity\Option")
     * @ORM\JoinColumn(nullable=false)
     *
     * @Assert\NotNull()
     */
    private $option;

    /**
     * @return AbstractOption|null
     */
    public function getOption(): ?AbstractOption
    {
        return $this->option;
    }

    /**
     * @param AbstractOption|null $option
     *
     * @return $this
     */
    public function setOption(?AbstractOption $option): self
    {
        $this->option = $option;

        return $this;
    }

    /**
     * @inheritDoc
     */
    public function distill()
    {
        return ['option' => $this->getOption()->getPermanentID()];
    }
}
