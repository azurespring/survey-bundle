<?php
/*
 * AbstractVector.php
 */

namespace AzureSpring\Bundle\SurveyBundle\Entity;

use AzureSpring\Bundle\SurveyBundle\Entity\Traits\LabelTrait;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
 */
abstract class AbstractVector extends AbstractQuestion implements LabelInterface
{
    use LabelTrait;

    /**
     * @ORM\OneToMany(targetEntity="AzureSpring\Bundle\SurveyBundle\Entity\FeatureInterest", mappedBy="question", orphanRemoval=true, cascade={"persist"})
     * @ORM\OrderBy({"serialNumber"="ASC"})
     */
    private $interests;

    /**
     * @ORM\OneToMany(targetEntity="AzureSpring\Bundle\SurveyBundle\Entity\VectorLabel", mappedBy="vector", orphanRemoval=true, cascade={"persist"})
     */
    private $localLabels;

    /**
     * Constructor.
     */
    public function __construct()
    {
        parent::__construct();

        $this->interests = new ArrayCollection();
        $this->localLabels = new ArrayCollection();
    }

    /**
     * @return Collection|FeatureInterest[]
     */
    public function getInterests(): Collection
    {
        return $this->interests;
    }

    /**
     * @param FeatureInterest $interest
     *
     * @return $this
     */
    public function addInterest(FeatureInterest $interest): self
    {
        if (!$this->interests->contains($interest)) {
            $this->interests[] = $interest;
            $interest->setQuestion($this);
            if (!$interest->getSerialNumber()) {
                $interest->setSerialNumber(count($this->interests));
            }
        }

        return $this;
    }

    /**
     * @param FeatureInterest $interest
     *
     * @return $this
     */
    public function removeInterest(FeatureInterest $interest): self
    {
        if ($this->interests->contains($interest)) {
            $this->interests->removeElement($interest);
            // set the owning side to null (unless already changed)
            if ($interest->getQuestion() === $this) {
                $interest->setQuestion(null);
            }
        }

        return $this;
    }

    /**
     * @inheritDoc
     */
    public function grabInterests()
    {
        return $this->getInterests()->toArray();
    }

    /**
     * @return Collection|VectorLabel[]
     */
    public function getLocalLabels(): Collection
    {
        return $this->localLabels;
    }

    /**
     * @param VectorLabel $localLabel
     *
     * @return $this
     */
    public function addLocalLabel(VectorLabel $localLabel): self
    {
        if (!$this->localLabels->contains($localLabel)) {
            $this->localLabels[] = $localLabel;
            $localLabel->setVector($this);
        }

        return $this;
    }

    /**
     * @param VectorLabel $localLabel
     *
     * @return $this
     */
    public function removeLocalLabel(VectorLabel $localLabel): self
    {
        if ($this->localLabels->contains($localLabel)) {
            $this->localLabels->removeElement($localLabel);
            // set the owning side to null (unless already changed)
            if ($localLabel->getVector() === $this) {
                $localLabel->setVector(null);
            }
        }

        return $this;
    }

    /**
     * @return VectorLabel
     */
    public function localizeLabel(): VectorLabel
    {
        $this->addLocalLabel($label = new VectorLabel());

        return $label;
    }
}
