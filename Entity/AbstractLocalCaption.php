<?php
/*
 * AbstractLocalCaption.php
 */

namespace AzureSpring\Bundle\SurveyBundle\Entity;

use AzureSpring\Bundle\SurveyBundle\Entity\Traits\LocaleTrait;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
 * @ORM\InheritanceType("JOINED")
 */
abstract class AbstractLocalCaption
{
    use LocaleTrait;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="json_array")
     */
    private $captions;

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getCaptions()
    {
        return $this->captions;
    }

    /**
     * @param mixed $captions
     *
     * @return $this
     */
    public function setCaptions($captions): self
    {
        $this->captions = $captions;

        return $this;
    }
}
