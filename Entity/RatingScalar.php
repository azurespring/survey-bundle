<?php
/*
 * RatingScalar.php
 */

namespace AzureSpring\Bundle\SurveyBundle\Entity;

use AzureSpring\Bundle\SurveyBundle\Entity\Traits\RatingTrait;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
 */
class RatingScalar extends AbstractScalar implements CaptionInterface
{
    use RatingTrait;

    /**
     * @ORM\OneToMany(targetEntity="AzureSpring\Bundle\SurveyBundle\Entity\ScalarCaption", mappedBy="rating", orphanRemoval=true, cascade={"persist"})
     */
    private $localCaptions;

    /**
     * Constructor.
     */
    public function __construct()
    {
        parent::__construct();

        $this->localCaptions = new ArrayCollection();
    }

    /**
     * @return Collection|ScalarCaption[]
     */
    public function getLocalCaptions(): Collection
    {
        return $this->localCaptions;
    }

    /**
     * @param ScalarCaption $localCaption
     *
     * @return $this
     */
    public function addLocalCaption(ScalarCaption $localCaption): self
    {
        if (!$this->localCaptions->contains($localCaption)) {
            $this->localCaptions[] = $localCaption;
            $localCaption->setRating($this);
        }

        return $this;
    }

    /**
     * @param ScalarCaption $localCaption
     *
     * @return $this
     */
    public function removeLocalCaption(ScalarCaption $localCaption): self
    {
        if ($this->localCaptions->contains($localCaption)) {
            $this->localCaptions->removeElement($localCaption);
            // set the owning side to null (unless already changed)
            if ($localCaption->getRating() === $this) {
                $localCaption->setRating(null);
            }
        }

        return $this;
    }

    /**
     * @return ScalarCaption
     */
    public function localizeCaption(): ScalarCaption
    {
        $this->addLocalCaption($caption = new ScalarCaption());

        return $caption;
    }
}
