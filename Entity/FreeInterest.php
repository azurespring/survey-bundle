<?php
/*
 * FreeInterest.php
 */

namespace AzureSpring\Bundle\SurveyBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;

/**
 * @ORM\Entity()
 */
class FreeInterest extends AbstractInterest
{
    /**
     * @ORM\OneToOne(targetEntity="AzureSpring\Bundle\SurveyBundle\Entity\AbstractScalar", inversedBy="interest", cascade={"persist", "remove"})
     * @ORM\JoinColumn(nullable=false)
     *
     * @Serializer\Exclude()
     */
    private $question;

    /**
     * @return AbstractScalar|null
     */
    public function getQuestion(): ?AbstractScalar
    {
        return $this->question;
    }

    /**
     * @param AbstractScalar $question
     *
     * @return $this
     */
    public function setQuestion(AbstractScalar $question): self
    {
        $this->question = $question;

        return $this;
    }
}
