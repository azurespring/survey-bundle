<?php
/*
 * Option.php
 */

namespace AzureSpring\Bundle\SurveyBundle\Entity;

use AzureSpring\Bundle\SurveyBundle\Entity\Traits\LabelTrait;
use AzureSpring\Bundle\SurveyBundle\Entity\Traits\PermanentIDTrait;
use AzureSpring\Bundle\SurveyBundle\Entity\Traits\SerialNumberTrait;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;

/**
 * @ORM\Entity()
 * @ORM\InheritanceType("JOINED")
 */
abstract class AbstractOption implements LabelInterface
{
    use PermanentIDTrait, SerialNumberTrait, LabelTrait;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="AzureSpring\Bundle\SurveyBundle\Entity\OptionGroup", inversedBy="children")
     *
     * @Serializer\Exclude()
     */
    private $parent;

    /**
     * @ORM\OneToMany(targetEntity="AzureSpring\Bundle\SurveyBundle\Entity\OptionLabel", mappedBy="option", orphanRemoval=true, cascade={"persist"})
     */
    private $localLabels;

    /**
     * Constructor.
     */
    public function __construct()
    {
        $this->localLabels = new ArrayCollection();
    }

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return OptionGroup|null
     */
    public function getParent(): ?OptionGroup
    {
        return $this->parent;
    }

    /**
     * @param OptionGroup|null $parent
     *
     * @return $this
     */
    public function setParent(?OptionGroup $parent): self
    {
        $this->parent = $parent;

        return $this;
    }

    /**
     * @return Collection|OptionLabel[]
     */
    public function getLocalLabels(): Collection
    {
        return $this->localLabels;
    }

    /**
     * @param OptionLabel $localLabel
     *
     * @return $this
     */
    public function addLocalLabel(OptionLabel $localLabel): self
    {
        if (!$this->localLabels->contains($localLabel)) {
            $this->localLabels[] = $localLabel;
            $localLabel->setOption($this);
        }

        return $this;
    }

    /**
     * @param OptionLabel $localLabel
     *
     * @return $this
     */
    public function removeLocalLabel(OptionLabel $localLabel): self
    {
        if ($this->localLabels->contains($localLabel)) {
            $this->localLabels->removeElement($localLabel);
            // set the owning side to null (unless already changed)
            if ($localLabel->getOption() === $this) {
                $localLabel->setOption(null);
            }
        }

        return $this;
    }

    /**
     * @return OptionLabel
     */
    public function localizeLabel(): OptionLabel
    {
        $this->addLocalLabel($label = new OptionLabel());

        return $label;
    }

    /**
     * @inheritDoc
     */
    public function __toString()
    {
        return "\"{$this->getLabel()}\" <{$this->getPermanentID()}>";
    }
}
