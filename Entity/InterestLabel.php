<?php
/*
 * InterestLabel.php
 */

namespace AzureSpring\Bundle\SurveyBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
 */
class InterestLabel extends AbstractLocalLabel
{
    /**
     * @ORM\ManyToOne(targetEntity="AzureSpring\Bundle\SurveyBundle\Entity\AbstractInterest", inversedBy="localLabels")
     * @ORM\JoinColumn(referencedColumnName="permanent_id", nullable=false)
     */
    private $interest;

    /**
     * @return AbstractInterest|null
     */
    public function getInterest(): ?AbstractInterest
    {
        return $this->interest;
    }

    /**
     * @param AbstractInterest|null $interest
     *
     * @return $this
     */
    public function setInterest(?AbstractInterest $interest): self
    {
        $this->interest = $interest;

        return $this;
    }
}
