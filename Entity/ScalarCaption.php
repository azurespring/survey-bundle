<?php
/*
 * ScalarCaption.php
 */

namespace AzureSpring\Bundle\SurveyBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
 */
class ScalarCaption extends AbstractLocalCaption
{
    /**
     * @ORM\ManyToOne(targetEntity="AzureSpring\Bundle\SurveyBundle\Entity\RatingScalar", inversedBy="localCaptions")
     * @ORM\JoinColumn(nullable=false)
     */
    private $rating;

    /**
     * @return RatingScalar|null
     */
    public function getRating(): ?RatingScalar
    {
        return $this->rating;
    }

    /**
     * @param RatingScalar|null $rating
     *
     * @return $this
     */
    public function setRating(?RatingScalar $rating): self
    {
        $this->rating = $rating;

        return $this;
    }
}
