<?php
/*
 * AbstractField.php
 */

namespace AzureSpring\Bundle\SurveyBundle\Entity;

use AzureSpring\Bundle\SurveyBundle\Entity\Traits\PermanentIDTrait;
use AzureSpring\Bundle\SurveyBundle\Entity\Traits\SerialNumberTrait;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
 * @ORM\InheritanceType("JOINED")
 */
abstract class AbstractField
{
    use PermanentIDTrait, SerialNumberTrait;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="AzureSpring\Bundle\SurveyBundle\Entity\Section", inversedBy="children")
     */
    private $parent;

    /**
     * @ORM\Column(type="boolean")
     */
    private $required;

    /**
     * @ORM\Column(type="datetimetz_immutable")
     */
    private $createdAt;

    /**
     * @ORM\OneToMany(targetEntity="AzureSpring\Bundle\SurveyBundle\Entity\Footnote", mappedBy="field", orphanRemoval=true, cascade={"persist"})
     * @ORM\OrderBy({"id"="ASC"})
     */
    private $footnotes;

    /**
     * Constructor.
     */
    public function __construct()
    {
        $this->footnotes = new ArrayCollection();
    }

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return bool|null
     */
    public function isRequired(): ?bool
    {
        return $this->required;
    }

    /**
     * @param bool $required
     *
     * @return $this
     */
    public function setRequired(bool $required): self
    {
        $this->required = $required;

        return $this;
    }

    public function isMandatory(): bool
    {
        return $this->isRequired() && (
            !$this->getParent() || $this->getParent()->isMandatory()
        );
    }

    /**
     * @return Section|null
     */
    public function getParent(): ?Section
    {
        return $this->parent;
    }

    /**
     * @param Section|null $parent
     *
     * @return $this
     */
    public function setParent(?Section $parent): self
    {
        $this->parent = $parent;

        return $this;
    }

    /**
     * @return \DateTimeImmutable|null
     */
    public function getCreatedAt(): ?\DateTimeImmutable
    {
        return $this->createdAt;
    }

    /**
     * @param \DateTimeImmutable $createdAt
     *
     * @return $this
     */
    public function setCreatedAt(\DateTimeImmutable $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * @return Collection|Footnote[]
     */
    public function getFootnotes(): Collection
    {
        return $this->footnotes;
    }

    /**
     * @param Footnote $footnote
     *
     * @return $this
     */
    public function addFootnote(Footnote $footnote): self
    {
        if (!$this->footnotes->contains($footnote)) {
            $this->footnotes[] = $footnote;
            $footnote->setField($this);
        }

        return $this;
    }

    /**
     * @param Footnote $footnote
     *
     * @return $this
     */
    public function removeFootnote(Footnote $footnote): self
    {
        if ($this->footnotes->contains($footnote)) {
            $this->footnotes->removeElement($footnote);
            // set the owning side to null (unless already changed)
            if ($footnote->getField() === $this) {
                $footnote->setField(null);
            }
        }

        return $this;
    }

    /**
     * @return string|null
     */
    abstract public function getLabel(): ?string;

    /**
     * @return AbstractInterest[]
     */
    abstract public function grabInterests();

    /**
     * @inheritDoc
     */
    public function __toString()
    {
        return "\"{$this->getLabel()}\" <{$this->getPermanentID()}>";
    }
}
