<?php
/*
 * Predicate.php
 */

namespace AzureSpring\Bundle\SurveyBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;

/**
 * @ORM\Entity()
 */
class Predicate extends AbstractScalar
{
    /**
     * @return string
     */
    public function getForm(): string
    {
        return self::PREDICATE;
    }

    /**
     * trait, for twig use.
     *
     * @return string
     *
     * @Serializer\VirtualProperty()
     */
    public function getPredicate(): string
    {
        return '~';
    }
}
