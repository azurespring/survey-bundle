<?php
/*
 * SerialNumberTrait.php
 */

namespace AzureSpring\Bundle\SurveyBundle\Entity\Traits;

use Doctrine\ORM\Mapping as ORM;

/**
 * SerialNumber
 */
trait SerialNumberTrait
{
    /**
     * @ORM\Column(type="integer")
     */
    private $serialNumber;

    /**
     * @return int|null
     */
    public function getSerialNumber(): ?int
    {
        return $this->serialNumber;
    }

    /**
     * @param int $serialNumber
     *
     * @return $this
     */
    public function setSerialNumber(int $serialNumber): self
    {
        $this->serialNumber = $serialNumber;

        return $this;
    }
}
