<?php
/*
 * OptionsTrait.php
 */

namespace AzureSpring\Bundle\SurveyBundle\Entity\Traits;

use AzureSpring\Bundle\SurveyBundle\Entity\AbstractQuestion;
use AzureSpring\Bundle\SurveyBundle\Entity\OptionGroup;
use Doctrine\ORM\Mapping as ORM;

/**
 * Options
 */
trait OptionsTrait
{
    /**
     * @ORM\OneToOne(targetEntity="AzureSpring\Bundle\SurveyBundle\Entity\OptionGroup", cascade={"persist", "remove"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $options;

    /**
     * @ORM\Column(type="boolean")
     */
    private $multiple;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $scratch;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $prompt;

    /**
     * @return OptionGroup|null
     */
    public function getOptions(): ?OptionGroup
    {
        return $this->options;
    }

    /**
     * @param OptionGroup $options
     *
     * @return $this
     */
    public function setOptions(OptionGroup $options): self
    {
        $this->options = $options;
        $options
            ->setSerialNumber(-1)
            ->setLabel('/')
        ;

        return $this;
    }

    /**
     * @return bool|null
     */
    public function getMultiple(): ?bool
    {
        return $this->multiple;
    }

    /**
     * @param bool $multiple
     *
     * @return $this
     */
    public function setMultiple(bool $multiple): self
    {
        $this->multiple = $multiple;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getScratch(): ?string
    {
        return $this->scratch;
    }

    /**
     * @param string|null $scratch
     *
     * @return $this
     */
    public function setScratch(?string $scratch): self
    {
        $this->scratch = $scratch;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getPrompt(): ?string
    {
        return $this->prompt;
    }

    /**
     * @param string|null $prompt
     *
     * @return $this
     */
    public function setPrompt(?string $prompt): self
    {
        $this->prompt = $prompt;

        return $this;
    }

    /**
     * @return string
     */
    public function getForm(): string
    {
        return AbstractQuestion::CHOICE;
    }
}
