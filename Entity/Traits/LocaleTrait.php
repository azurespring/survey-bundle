<?php
/*
 * LocaleTrait.php
 */

namespace AzureSpring\Bundle\SurveyBundle\Entity\Traits;

use AzureSpring\Bundle\SurveyBundle\Model\LocaleInterface;
use Doctrine\ORM\Mapping as ORM;

/**
 * Locale
 */
trait LocaleTrait
{
    /**
     * @ORM\ManyToOne(targetEntity="AzureSpring\Bundle\SurveyBundle\Model\LocaleInterface")
     * @ORM\JoinColumn(nullable=false)
     */
    private $locale;

    /**
     * @return LocaleInterface|null
     */
    public function getLocale(): ?LocaleInterface
    {
        return $this->locale;
    }

    /**
     * @param LocaleInterface|null $locale
     *
     * @return $this
     */
    public function setLocale(?LocaleInterface $locale): self
    {
        $this->locale = $locale;

        return $this;
    }
}
