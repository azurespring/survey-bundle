<?php
/*
 * PermanentIDTrait.php
 */

namespace AzureSpring\Bundle\SurveyBundle\Entity\Traits;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;

/**
 * PermanentID
 */
trait PermanentIDTrait
{
    /**
     * @ORM\Column(type="string", length=255, unique=true)
     *
     * @Serializer\SerializedName("permanent_id")
     */
    protected $permanentID;

    /**
     * @return string|null
     */
    public function getPermanentID(): ?string
    {
        return $this->permanentID;
    }

    /**
     * @param string $permanentID
     *
     * @return $this
     */
    public function setPermanentID(string $permanentID): self
    {
        $this->permanentID = $permanentID;

        return $this;
    }
}
