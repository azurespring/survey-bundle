<?php
/*
 * LabelTrait.php
 */

namespace AzureSpring\Bundle\SurveyBundle\Entity\Traits;

use Doctrine\ORM\Mapping as ORM;

/**
 * Label
 */
trait LabelTrait
{
    /**
     * @ORM\Column(type="string", length=255)
     */
    private $label;

    /**
     * @return string|null
     */
    public function getLabel(): ?string
    {
        return $this->label;
    }

    /**
     * @param string $label
     *
     * @return $this
     */
    public function setLabel(string $label): self
    {
        $this->label = $label;

        return $this;
    }
}
