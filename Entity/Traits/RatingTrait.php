<?php
/*
 * RatingSuiteTrait
 */

namespace AzureSpring\Bundle\SurveyBundle\Entity\Traits;

use AzureSpring\Bundle\SurveyBundle\Entity\AbstractQuestion;
use Doctrine\ORM\Mapping as ORM;

/**
 * Rating
 */
trait RatingTrait
{
    /**
     * @ORM\Column(type="integer")
     */
    private $scale;

    /**
     * @ORM\Column(type="json_array")
     */
    private $captions;

    /**
     * @return int|null
     */
    public function getScale(): ?int
    {
        return $this->scale;
    }

    /**
     * @param int $scale
     *
     * @return $this
     */
    public function setScale(int $scale): self
    {
        $this->scale = $scale;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getCaptions()
    {
        return $this->captions;
    }

    /**
     * @param mixed $captions
     *
     * @return $this
     */
    public function setCaptions($captions): self
    {
        $this->captions = $captions;

        return $this;
    }

    /**
     * @return string
     */
    public function getForm(): string
    {
        return AbstractQuestion::RATING;
    }
}
