<?php
/*
 * AbstractScalar.php
 */

namespace AzureSpring\Bundle\SurveyBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
 */
abstract class AbstractScalar extends AbstractQuestion
{
    /**
     * @ORM\OneToOne(targetEntity="AzureSpring\Bundle\SurveyBundle\Entity\FreeInterest", mappedBy="question", orphanRemoval=true, cascade={"persist", "remove"})
     */
    private $interest;

    /**
     * @return FreeInterest|null
     */
    public function getInterest(): ?FreeInterest
    {
        return $this->interest;
    }

    /**
     * @param FreeInterest $interest
     *
     * @return $this
     */
    public function setInterest(FreeInterest $interest): self
    {
        $this->interest = $interest;

        // set the owning side of the relation if necessary
        if ($this !== $interest->getQuestion()) {
            $interest->setQuestion($this);
        }

        return $this;
    }

    /**
     * @inheritDoc
     */
    public function getLabel(): ?string
    {
        return $this->getInterest()->getLabel();
    }

    /**
     * @inheritDoc
     */
    public function grabInterests()
    {
        return [$this->getInterest()];
    }
}
