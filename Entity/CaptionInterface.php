<?php
/*
 * CaptionInterface.php
 */

namespace AzureSpring\Bundle\SurveyBundle\Entity;

use Doctrine\Common\Collections\Collection;

/**
 * CaptionInterface
 */
interface CaptionInterface
{
    /**
     * @return mixed
     */
    public function getCaptions();

    /**
     * @return Collection|AbstractLocalCaption[]
     */
    public function getLocalCaptions(): Collection;

    /**
     * @return AbstractLocalCaption
     */
    public function localizeCaption();
}
