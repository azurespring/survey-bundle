<?php
/*
 * MultiAnswerSolution.php
 */

namespace AzureSpring\Bundle\SurveyBundle\Entity;

use AzureSpring\Bundle\SurveyBundle\Form\Model\ChoiceSolutionInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity()
 *
 * @method ChoiceScalar|ChoiceVector getQuestion()
 *
 * @Assert\Expression("this.getOptions().count() > 0 || this.getParagraph()")
 */
class MultiAnswerSolution extends AbstractSolution implements ChoiceSolutionInterface
{
    /**
     * @ORM\ManyToMany(targetEntity="AzureSpring\Bundle\SurveyBundle\Entity\Option")
     */
    private $options;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $paragraph;

    /**
     * Constructor.
     */
    public function __construct()
    {
        $this->options = new ArrayCollection();
    }

    /**
     * @return Collection|Option[]
     */
    public function getOptions(): Collection
    {
        return $this->options;
    }

    /**
     * @param Option $option
     *
     * @return $this
     */
    public function addOption(Option $option): self
    {
        if (!$this->options->contains($option)) {
            $this->options[] = $option;
        }

        return $this;
    }

    /**
     * @param Option $option
     *
     * @return $this
     */
    public function removeOption(Option $option): self
    {
        if ($this->options->contains($option)) {
            $this->options->removeElement($option);
        }

        return $this;
    }

    /**
     * @return string|null
     */
    public function getParagraph(): ?string
    {
        return $this->paragraph;
    }

    /**
     * @param string|null $paragraph
     *
     * @return $this
     */
    public function setParagraph(?string $paragraph): self
    {
        $this->paragraph = $paragraph;

        return $this;
    }

    /**
     * @inheritDoc
     */
    public function distill()
    {
        return [
            'options' => array_map(
                function (Option $option) {
                    return $option->getPermanentID();
                },
                $this->getOptions()->toArray()
            ),
        ] + ($this->getQuestion()->getScratch() ? [
            'paragraph' => $this->getParagraph(),
        ] : [
        ]);
    }
}
