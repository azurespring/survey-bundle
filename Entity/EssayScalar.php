<?php
/*
 * EssayScalar.php
 */

namespace AzureSpring\Bundle\SurveyBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
 */
class EssayScalar extends AbstractScalar
{
    /**
     * @inheritDoc
     */
    public function getForm(): string
    {
        return self::ESSAY;
    }
}
