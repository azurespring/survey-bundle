<?php
/*
 * AbstractSolution.php
 */

namespace AzureSpring\Bundle\SurveyBundle\Entity;

use AzureSpring\Bundle\SurveyBundle\Model\AbstractCase;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
 * @ORM\InheritanceType("JOINED")
 */
abstract class AbstractSolution
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="AzureSpring\Bundle\SurveyBundle\Model\AbstractCase", inversedBy="solutions")
     * @ORM\JoinColumn(nullable=false)
     */
    private $case;

    /**
     * @ORM\ManyToOne(targetEntity="AzureSpring\Bundle\SurveyBundle\Entity\AbstractInterest")
     * @ORM\JoinColumn(referencedColumnName="permanent_id", nullable=false)
     */
    private $interest;

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return AbstractCase|null
     */
    public function getCase(): ?AbstractCase
    {
        return $this->case;
    }

    /**
     * @param AbstractCase|null $case
     *
     * @return $this
     */
    public function setCase(?AbstractCase $case): self
    {
        $this->case = $case;

        return $this;
    }

    /**
     * @return AbstractInterest|null
     */
    public function getInterest(): ?AbstractInterest
    {
        return $this->interest;
    }

    /**
     * @param AbstractInterest|null $interest
     *
     * @return $this
     */
    public function setInterest(?AbstractInterest $interest): self
    {
        $this->interest = $interest;

        return $this;
    }

    /**
     * @return string
     */
    public function getPermanentID(): string
    {
        return $this->getInterest()->getPermanentID();
    }

    /**
     * @return AbstractQuestion
     */
    public function getQuestion(): AbstractQuestion
    {
        return $this->getInterest()->getQuestion();
    }

    /**
     * @return mixed
     */
    abstract public function distill();
}
