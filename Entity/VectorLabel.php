<?php
/*
 * VectorLabel.php
 */

namespace AzureSpring\Bundle\SurveyBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
 */
class VectorLabel extends AbstractLocalLabel
{
    /**
     * @ORM\ManyToOne(targetEntity="AzureSpring\Bundle\SurveyBundle\Entity\AbstractVector", inversedBy="localLabels")
     * @ORM\JoinColumn(nullable=false)
     */
    private $vector;

    /**
     * @return AbstractVector|null
     */
    public function getVector(): ?AbstractVector
    {
        return $this->vector;
    }

    /**
     * @param AbstractVector|null $vector
     *
     * @return $this
     */
    public function setVector(?AbstractVector $vector): self
    {
        $this->vector = $vector;

        return $this;
    }
}
