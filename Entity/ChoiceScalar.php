<?php
/*
 * ChoiceScalar.php
 */

namespace AzureSpring\Bundle\SurveyBundle\Entity;

use AzureSpring\Bundle\SurveyBundle\Entity\Traits\OptionsTrait;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
 */
class ChoiceScalar extends AbstractScalar
{
    use OptionsTrait;

    /**
     * @ORM\OneToMany(targetEntity="AzureSpring\Bundle\SurveyBundle\Entity\ScratchLabel", mappedBy="choice", orphanRemoval=true, cascade={"persist"})
     */
    private $localScratches;

    /**
     * Constructor.
     */
    public function __construct()
    {
        parent::__construct();

        $this->localScratches = new ArrayCollection();
    }

    /**
     * @return Collection|ScratchLabel[]
     */
    public function getLocalScratches(): Collection
    {
        return $this->localScratches;
    }

    /**
     * @param ScratchLabel $localScratch
     *
     * @return $this
     */
    public function addLocalScratch(ScratchLabel $localScratch): self
    {
        if (!$this->localScratches->contains($localScratch)) {
            $this->localScratches[] = $localScratch;
            $localScratch->setChoice($this);
        }

        return $this;
    }

    /**
     * @param ScratchLabel $localScratch
     *
     * @return $this
     */
    public function removeLocalScratch(ScratchLabel $localScratch): self
    {
        if ($this->localScratches->contains($localScratch)) {
            $this->localScratches->removeElement($localScratch);
            // set the owning side to null (unless already changed)
            if ($localScratch->getChoice() === $this) {
                $localScratch->setChoice(null);
            }
        }

        return $this;
    }

    /**
     * @return ScratchLabel
     */
    public function localizeScratch()
    {
        $this->addLocalScratch($local = new ScratchLabel());

        return $local;
    }
}
