<?php
/*
 * OptionLabel.php
 */

namespace AzureSpring\Bundle\SurveyBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
 */
class OptionLabel extends AbstractLocalLabel
{
    /**
     * @ORM\ManyToOne(targetEntity="AzureSpring\Bundle\SurveyBundle\Entity\AbstractOption", inversedBy="localLabels")
     * @ORM\JoinColumn(nullable=false)
     */
    private $option;

    /**
     * @return AbstractOption|null
     */
    public function getOption(): ?AbstractOption
    {
        return $this->option;
    }

    /**
     * @param AbstractOption|null $option
     *
     * @return $this
     */
    public function setOption(?AbstractOption $option): self
    {
        $this->option = $option;

        return $this;
    }
}
