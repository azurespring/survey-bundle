<?php
/*
 * ScratchLabel.php
 */

namespace AzureSpring\Bundle\SurveyBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
 */
class ScratchLabel extends AbstractLocalLabel
{
    /**
     * @ORM\ManyToOne(targetEntity="AzureSpring\Bundle\SurveyBundle\Entity\ChoiceScalar", inversedBy="localScratches")
     * @ORM\JoinColumn(nullable=false)
     */
    private $choice;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $prompt;

    /**
     * @return ChoiceScalar|null
     */
    public function getChoice(): ?ChoiceScalar
    {
        return $this->choice;
    }

    /**
     * @param ChoiceScalar|null $choice
     *
     * @return $this
     */
    public function setChoice(?ChoiceScalar $choice): self
    {
        $this->choice = $choice;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getPrompt(): ?string
    {
        return $this->prompt;
    }

    /**
     * @param string|null $prompt
     *
     * @return $this
     */
    public function setPrompt(?string $prompt): self
    {
        $this->prompt = $prompt;

        return $this;
    }
}
