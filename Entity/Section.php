<?php
/*
 * Section.php
 */

namespace AzureSpring\Bundle\SurveyBundle\Entity;

use AzureSpring\Bundle\SurveyBundle\Entity\Traits\LabelTrait;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
 */
class Section extends AbstractField implements LabelInterface
{
    use LabelTrait;

    /**
     * @ORM\OneToMany(targetEntity="AzureSpring\Bundle\SurveyBundle\Entity\AbstractField", mappedBy="parent", cascade={"persist"})
     * @ORM\OrderBy({"serialNumber"="ASC"})
     */
    private $children;

    /**
     * @ORM\OneToMany(targetEntity="AzureSpring\Bundle\SurveyBundle\Entity\SectionLabel", mappedBy="section", orphanRemoval=true, cascade={"persist"})
     */
    private $localLabels;

    /**
     * Constructor.
     */
    public function __construct()
    {
        parent::__construct();

        $this->children = new ArrayCollection();
        $this->localLabels = new ArrayCollection();
    }

    /**
     * @return Collection|AbstractField[]
     */
    public function getChildren(): Collection
    {
        return $this->children;
    }

    /**
     * @param AbstractField $child
     *
     * @return $this
     */
    public function addChild(AbstractField $child): self
    {
        if (!$this->children->contains($child)) {
            $this->children[] = $child;
            $child->setParent($this);
            if (!$child->getSerialNumber()) {
                $child->setSerialNumber(count($this->children));
            }
        }

        return $this;
    }

    /**
     * @param AbstractField $child
     *
     * @return $this
     */
    public function removeChild(AbstractField $child): self
    {
        if ($this->children->contains($child)) {
            $this->children->removeElement($child);
            // set the owning side to null (unless already changed)
            if ($child->getParent() === $this) {
                $child->setParent(null);
            }
        }

        return $this;
    }

    /**
     * @inheritDoc
     *
     * @return Section
     */
    public function setCreatedAt(\DateTimeImmutable $createdAt): AbstractField
    {
        foreach ($this->children as $child) {
            $child->setCreatedAt($createdAt);
        }

        return parent::setCreatedAt($createdAt);
    }

    /**
     * @return Collection|SectionLabel[]
     */
    public function getLocalLabels(): Collection
    {
        return $this->localLabels;
    }

    /**
     * @param SectionLabel $localLabel
     *
     * @return $this
     */
    public function addLocalLabel(SectionLabel $localLabel): self
    {
        if (!$this->localLabels->contains($localLabel)) {
            $this->localLabels[] = $localLabel;
            $localLabel->setSection($this);
        }

        return $this;
    }

    /**
     * @param SectionLabel $localLabel
     *
     * @return $this
     */
    public function removeLocalLabel(SectionLabel $localLabel): self
    {
        if ($this->localLabels->contains($localLabel)) {
            $this->localLabels->removeElement($localLabel);
            // set the owning side to null (unless already changed)
            if ($localLabel->getSection() === $this) {
                $localLabel->setSection(null);
            }
        }

        return $this;
    }

    /**
     * @return SectionLabel
     */
    public function localizeLabel(): SectionLabel
    {
        $this->addLocalLabel($label = new SectionLabel());

        return $label;
    }

    /**
     * @inheritDoc
     */
    public function grabInterests()
    {
        return call_user_func_array('array_merge', array_map(
            function (AbstractField $f) {
                return $f->grabInterests();
            },
            $this->getChildren()->toArray()
        ) ?: [[]]);
    }
}
