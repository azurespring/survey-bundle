<?php
/*
 * RatingSolution.php
 */

namespace AzureSpring\Bundle\SurveyBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity()
 *
 * @method RatingScalar|RatingVector getQuestion()
 */
class RatingSolution extends AbstractSolution
{
    /**
     * @ORM\Column(type="integer")
     *
     * @Assert\NotBlank()
     * @Assert\Expression("0 < this.getRating() && this.getRating() <= this.getInterest().getQuestion().getScale()")
     */
    private $rating;

    /**
     * @return int|null
     */
    public function getRating(): ?int
    {
        return $this->rating;
    }

    /**
     * @param int $rating
     *
     * @return $this
     */
    public function setRating(int $rating): self
    {
        $this->rating = $rating;

        return $this;
    }

    /**
     * @inheritDoc
     */
    public function distill()
    {
        return ['rating' => $this->getRating()];
    }
}
