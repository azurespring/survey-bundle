<?php
/*
 * AbstractQuestion.php
 */

namespace AzureSpring\Bundle\SurveyBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
 */
abstract class AbstractQuestion extends AbstractField
{
    const CHOICE    = 'CHOICE';
    const ESSAY     = 'ESSAY';
    const RATING    = 'RATING';
    const PREDICATE = 'PREDICATE';

    /**
     * @return string
     */
    abstract public function getForm(): string;
}
