<?php
/*
 * ChoiceVector.php
 */

namespace AzureSpring\Bundle\SurveyBundle\Entity;

use AzureSpring\Bundle\SurveyBundle\Entity\Traits\OptionsTrait;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
 */
class ChoiceVector extends AbstractVector
{
    use OptionsTrait;
}
