<?php
/*
 * FeatureInterest.php
 */

namespace AzureSpring\Bundle\SurveyBundle\Entity;

use AzureSpring\Bundle\SurveyBundle\Entity\Traits\SerialNumberTrait;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;

/**
 * @ORM\Entity()
 */
class FeatureInterest extends AbstractInterest
{
    use SerialNumberTrait;

    /**
     * @ORM\ManyToOne(targetEntity="AzureSpring\Bundle\SurveyBundle\Entity\AbstractVector", inversedBy="interests")
     * @ORM\JoinColumn(nullable=false)
     *
     * @Serializer\Exclude()
     */
    private $question;

    /**
     * @return AbstractVector|null
     */
    public function getQuestion(): ?AbstractVector
    {
        return $this->question;
    }

    /**
     * @param AbstractVector|null $question
     *
     * @return $this
     */
    public function setQuestion(?AbstractVector $question): self
    {
        $this->question = $question;

        return $this;
    }
}
