<?php
/*
 * Footnote.php
 */

namespace AzureSpring\Bundle\SurveyBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
 */
class Footnote
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="AzureSpring\Bundle\SurveyBundle\Entity\AbstractField", inversedBy="footnotes")
     * @ORM\JoinColumn(nullable=false)
     */
    private $field;

    /**
     * @ORM\Column(type="text")
     */
    private $paragraph;

    /**
     * @ORM\OneToMany(targetEntity="AzureSpring\Bundle\SurveyBundle\Entity\LocalParagraph", mappedBy="footnote", orphanRemoval=true, cascade={"persist"})
     */
    private $localParagraphs;

    /**
     * Constructor.
     */
    public function __construct()
    {
        $this->localParagraphs = new ArrayCollection();
    }

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return AbstractField|null
     */
    public function getField(): ?AbstractField
    {
        return $this->field;
    }

    /**
     * @param AbstractField|null $field
     *
     * @return $this
     */
    public function setField(?AbstractField $field): self
    {
        $this->field = $field;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getParagraph(): ?string
    {
        return $this->paragraph;
    }

    /**
     * @param string $paragraph
     *
     * @return $this
     */
    public function setParagraph(string $paragraph): self
    {
        $this->paragraph = $paragraph;

        return $this;
    }

    /**
     * @return Collection|LocalParagraph[]
     */
    public function getLocalParagraphs(): Collection
    {
        return $this->localParagraphs;
    }

    /**
     * @param LocalParagraph $localParagraph
     *
     * @return $this
     */
    public function addLocalParagraph(LocalParagraph $localParagraph): self
    {
        if (!$this->localParagraphs->contains($localParagraph)) {
            $this->localParagraphs[] = $localParagraph;
            $localParagraph->setFootnote($this);
        }

        return $this;
    }

    /**
     * @param LocalParagraph $localParagraph
     *
     * @return $this
     */
    public function removeLocalParagraph(LocalParagraph $localParagraph): self
    {
        if ($this->localParagraphs->contains($localParagraph)) {
            $this->localParagraphs->removeElement($localParagraph);
            // set the owning side to null (unless already changed)
            if ($localParagraph->getFootnote() === $this) {
                $localParagraph->setFootnote(null);
            }
        }

        return $this;
    }

    /**
     * @return LocalParagraph
     */
    public function localizeParagraph(): LocalParagraph
    {
        $this->addLocalParagraph($paragraph = new LocalParagraph());

        return $paragraph;
    }
}
