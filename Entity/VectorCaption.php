<?php
/*
 * VectorCaption.php
 */
namespace AzureSpring\Bundle\SurveyBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
 */
class VectorCaption extends AbstractLocalCaption
{
    /**
     * @ORM\ManyToOne(targetEntity="AzureSpring\Bundle\SurveyBundle\Entity\RatingVector", inversedBy="localCaptions")
     * @ORM\JoinColumn(nullable=false)
     */
    private $rating;

    /**
     * @return RatingVector|null
     */
    public function getRating(): ?RatingVector
    {
        return $this->rating;
    }

    /**
     * @param RatingVector|null $rating
     *
     * @return $this
     */
    public function setRating(?RatingVector $rating): self
    {
        $this->rating = $rating;

        return $this;
    }
}
