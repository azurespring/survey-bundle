<?php
/*
 * LocalFootnote.php
 */

namespace AzureSpring\Bundle\SurveyBundle\Entity;

use AzureSpring\Bundle\SurveyBundle\Entity\Traits\LocaleTrait;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
 * @ORM\InheritanceType("JOINED")
 */
class LocalParagraph
{
    use LocaleTrait;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="AzureSpring\Bundle\SurveyBundle\Entity\Footnote", inversedBy="localParagraphs")
     * @ORM\JoinColumn(nullable=false)
     */
    private $footnote;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $paragraph;

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return Footnote|null
     */
    public function getFootnote(): ?Footnote
    {
        return $this->footnote;
    }

    /**
     * @param Footnote|null $footnote
     *
     * @return $this
     */
    public function setFootnote(?Footnote $footnote): self
    {
        $this->footnote = $footnote;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getParagraph(): ?string
    {
        return $this->paragraph;
    }

    /**
     * @param string $paragraph
     *
     * @return $this
     */
    public function setParagraph(string $paragraph): self
    {
        $this->paragraph = $paragraph;

        return $this;
    }
}
