<?php
/*
 * RatingVector.php
 */

namespace AzureSpring\Bundle\SurveyBundle\Entity;

use AzureSpring\Bundle\SurveyBundle\Entity\Traits\RatingTrait;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
 */
class RatingVector extends AbstractVector implements CaptionInterface
{
    use RatingTrait;

    /**
     * @ORM\OneToMany(targetEntity="AzureSpring\Bundle\SurveyBundle\Entity\VectorCaption", mappedBy="rating", orphanRemoval=true, cascade={"persist"})
     */
    private $localCaptions;

    /**
     * Constructor.
     */
    public function __construct()
    {
        parent::__construct();

        $this->localCaptions = new ArrayCollection();
    }

    /**
     * @return Collection|VectorCaption[]
     */
    public function getLocalCaptions(): Collection
    {
        return $this->localCaptions;
    }

    /**
     * @param VectorCaption $localCaption
     *
     * @return $this
     */
    public function addLocalCaption(VectorCaption $localCaption): self
    {
        if (!$this->localCaptions->contains($localCaption)) {
            $this->localCaptions[] = $localCaption;
            $localCaption->setRating($this);
        }

        return $this;
    }

    /**
     * @param VectorCaption $localCaption
     *
     * @return $this
     */
    public function removeLocalCaption(VectorCaption $localCaption): self
    {
        if ($this->localCaptions->contains($localCaption)) {
            $this->localCaptions->removeElement($localCaption);
            // set the owning side to null (unless already changed)
            if ($localCaption->getRating() === $this) {
                $localCaption->setRating(null);
            }
        }

        return $this;
    }

    /**
     * @return VectorCaption
     */
    public function localizeCaption(): VectorCaption
    {
        $this->addLocalCaption($caption = new VectorCaption());

        return $caption;
    }
}
