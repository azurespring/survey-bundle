<?php
/*
 * PredicateSolution.php
 */

namespace AzureSpring\Bundle\SurveyBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity()
 */
class PredicateSolution extends AbstractSolution
{
    /**
     * @ORM\Column(type="boolean")
     *
     * @Assert\IsTrue()
     */
    private $checked;

    /**
     * @return bool|null
     */
    public function getChecked(): ?bool
    {
        return $this->checked;
    }

    /**
     * @param bool $checked
     *
     * @return $this
     */
    public function setChecked(bool $checked): self
    {
        $this->checked = $checked;

        return $this;
    }

    /**
     * @inheritDoc
     */
    public function distill()
    {
        return [
            'checked' => $this->getChecked(),
        ];
    }
}
