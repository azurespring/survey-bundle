<?php
/*
 * LabelInterface.php
 */

namespace AzureSpring\Bundle\SurveyBundle\Entity;

use Doctrine\Common\Collections\Collection;

/**
 * LabelInterface
 */
interface LabelInterface
{
    /**
     * @return string|null
     */
    public function getLabel(): ?string;

    /**
     * @param string $label
     *
     * @return $this
     */
    public function setLabel(string $label);

    /**
     * @return Collection|AbstractLocalLabel[]
     */
    public function getLocalLabels(): Collection;

    /**
     * @return AbstractLocalLabel
     */
    public function localizeLabel();
}
