<?php
/*
 * OptionGroup.php
 */

namespace AzureSpring\Bundle\SurveyBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
 */
class OptionGroup extends AbstractOption
{
    /**
     * @ORM\OneToMany(targetEntity="AzureSpring\Bundle\SurveyBundle\Entity\AbstractOption", mappedBy="parent", cascade={"persist"})
     * @ORM\OrderBy({"serialNumber"="ASC"})
     */
    private $children;

    /**
     * Constructor.
     */
    public function __construct()
    {
        parent::__construct();

        $this->children = new ArrayCollection();
    }

    /**
     * @return Collection|AbstractOption[]
     */
    public function getChildren(): Collection
    {
        return $this->children;
    }

    /**
     * @param AbstractOption $child
     *
     * @return $this
     */
    public function addChild(AbstractOption $child): self
    {
        if (!$this->children->contains($child)) {
            $this->children[] = $child;
            $child->setParent($this);
            if (!$child->getSerialNumber()) {
                $child->setSerialNumber(count($this->children));
            }
        }

        return $this;
    }

    /**
     * @param AbstractOption $child
     *
     * @return $this
     */
    public function removeChild(AbstractOption $child): self
    {
        if ($this->children->contains($child)) {
            $this->children->removeElement($child);
            // set the owning side to null (unless already changed)
            if ($child->getParent() === $this) {
                $child->setParent(null);
            }
        }

        return $this;
    }

    /**
     * @return AbstractOption[]
     */
    public function rollOut(): array
    {
        $opts = [];
        foreach ($this->getChildren() as $opt) {
            if ($opt instanceof OptionGroup) {
                $opts = array_merge($opts, $opt->rollOut());
            } else {
                $opts[] = $opt;
            }
        }

        return $opts;
    }
}
