<?php
/*
 * SectionLabel.php
 */

namespace AzureSpring\Bundle\SurveyBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
 */
class SectionLabel extends AbstractLocalLabel
{
    /**
     * @ORM\ManyToOne(targetEntity="AzureSpring\Bundle\SurveyBundle\Entity\Section", inversedBy="localLabels")
     * @ORM\JoinColumn(nullable=false)
     */
    private $section;

    /**
     * @return Section|null
     */
    public function getSection(): ?Section
    {
        return $this->section;
    }

    /**
     * @param Section|null $section
     *
     * @return $this
     */
    public function setSection(?Section $section): self
    {
        $this->section = $section;

        return $this;
    }
}
