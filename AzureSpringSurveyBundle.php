<?php
/*
 * AzureSpringSurveyBundle.php
 */

namespace AzureSpring\Bundle\SurveyBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class AzureSpringSurveyBundle extends Bundle
{
}
