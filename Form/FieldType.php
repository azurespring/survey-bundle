<?php
/*
 * FieldType.php
 */

namespace AzureSpring\Bundle\SurveyBundle\Form;

use AzureSpring\Bundle\SurveyBundle\Form\EventSubscriber\FieldSubscriber;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * FieldType
 */
class FieldType extends AbstractType
{
    /**
     * @inheritDoc
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('permanentID')
            ->add('serialNumber')
            ->add('required', null, [
                'empty_data' => false,
            ])
            ->add('footnotes', CollectionType::class, [
                'entry_type'    => FootnoteType::class,
                'entry_options' => @$options['allow_extra_fields'] ? [
                    'allow_extra_fields' =>  true,
                ] : [],
                'allow_add'     => true,
                'allow_delete'  => true,
                'by_reference'  => false,
            ])
        ;

        $builder->addEventSubscriber(new FieldSubscriber());
    }

    /**
     * @inheritDoc
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => FieldSubscriber::class,
        ]);
    }
}
