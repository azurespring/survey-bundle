<?php
/*
 * CaseType.php
 */

namespace AzureSpring\Bundle\SurveyBundle\Form;

use AzureSpring\Bundle\SurveyBundle\Entity\AbstractInterest;
use AzureSpring\Bundle\SurveyBundle\Model\AbstractCase;
use AzureSpring\Bundle\SurveyBundle\Entity\AbstractQuestion;
use AzureSpring\Bundle\SurveyBundle\Entity\Section;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * AnswerSheetType
 */
class CaseType extends AbstractType
{
    /**
     * @inheritDoc
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $optInterests = [];
        $solutions = $builder->create('solutions', FormType::class, [
            'by_reference' => false,
        ]);

        /** @var AbstractInterest $interest */
        foreach ($options['section']->grabInterests() as $interest) {
            if (!$interest->getQuestion()->isMandatory()) {
                $optInterests[$interest->getPermanentID()] = $interest;

                continue;
            }

            $this->addInterest($solutions, $interest);
        }

        $solutions->addEventListener(FormEvents::PRE_SUBMIT, function (FormEvent $event) use ($optInterests) {
            $data = $event->getData();
            $form = $event->getForm();

            foreach (array_keys($data ?? []) as $key) {
                if ($interest = @$optInterests[$key]) {
                    $this->addInterest($form, $interest);
                }
            }
        });

        $builder->add($solutions);
    }

    /**
     * @inheritDoc
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => AbstractCase::class,
            'section'    => null,
        ]);
        $resolver->setAllowedTypes('section', Section::class);
    }

    /**
     * @param FormBuilderInterface|FormInterface $form
     * @param AbstractInterest                   $interest
     */
    private function addInterest($form, AbstractInterest $interest)
    {
        $form->add(
            $interest->getPermanentID(),
            [
                AbstractQuestion::CHOICE => ChoiceSolutionType::class,
                AbstractQuestion::ESSAY => EssaySolutionType::class,
                AbstractQuestion::RATING => RatingSolutionType::class,
                AbstractQuestion::PREDICATE => PredicateSolutionType::class,
            ][$interest->getQuestion()->getForm()],
            [
                'interest' => $interest,
                'property_path' => "[{$interest->getPermanentID()}]",
            ]
        );
    }
}
