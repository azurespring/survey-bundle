<?php
/*
 * OptionType.php
 */

namespace AzureSpring\Bundle\SurveyBundle\Form;

use AzureSpring\Bundle\SurveyBundle\Entity\OptionLabel;
use AzureSpring\Bundle\SurveyBundle\Form\EventSubscriber\OptionSubscriber;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * OptionType
 */
class OptionType extends AbstractType
{
    /**
     * @inheritDoc
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        LocalLabelType::addLocalLabels($builder, OptionLabel::class);
        $builder
            ->add('permanentID')
            ->add('serialNumber')
            ->add('label')
        ;

        $builder->addEventSubscriber(new OptionSubscriber());
    }

    /**
     * @inheritDoc
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => OptionSubscriber::class,
        ]);
    }
}
