<?php
/*
 * ChoiceSolutionType.php
 */

namespace AzureSpring\Bundle\SurveyBundle\Form;

use AzureSpring\Bundle\SurveyBundle\Entity\AbstractInterest;
use AzureSpring\Bundle\SurveyBundle\Entity\AbstractQuestion;
use AzureSpring\Bundle\SurveyBundle\Entity\ChoiceSolution;
use AzureSpring\Bundle\SurveyBundle\Form\Model\ChoiceSolutionInterface;
use AzureSpring\Bundle\SurveyBundle\Entity\MultiAnswerSolution;
use AzureSpring\Bundle\SurveyBundle\Entity\Option;
use AzureSpring\Bundle\SurveyBundle\Entity\Traits\OptionsTrait;
use AzureSpring\Bundle\SurveyBundle\Form\EventSubscriber\ChoiceSolutionSubscriber;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * ChoiceSolutionType
 */
class ChoiceSolutionType extends AbstractType
{
    /**
     * @inheritDoc
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        /** @var AbstractInterest $interest */
        $interest = $options['interest'];
        /** @var AbstractQuestion&OptionsTrait $question */
        $question = $interest->getQuestion();
        if ($question->getMultiple()) {
            $builder
                ->add('options', EntityType::class, [
                    'class'        => Option::class,
                    'choices'      => $question->getOptions()->rollOut(),
                    'choice_value' => 'permanentID',
                    'multiple'     => true,
                ])
            ;

            if ($question->getScratch()) {
                $builder->add('paragraph');
            }

            /*
             * Since there are multi-choice questions which are changed to multi-answers,
             * a check is necessary to switch solution type if necessary.
             */
            $builder->addEventListener(FormEvents::PRE_SET_DATA, function (FormEvent $event) use ($interest) {
                if ($event->getData() instanceof MultiAnswerSolution) {
                    return;
                }

                $event->setData(
                    (new MultiAnswerSolution())
                    ->setInterest($interest)
                );
            });
        } elseif (!$question->getScratch()) {
            $builder
                ->add('option', EntityType::class, [
                    'class'        => Option::class,
                    'choices'      => $question->getOptions()->rollOut(),
                    'choice_value' => 'permanentID',
                ])
            ;
        } else {
            $builder->addEventSubscriber(new ChoiceSolutionSubscriber());
        }
    }

    /**
     * @inheritDoc
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => ChoiceSolutionInterface::class,
            'interest'   => null,
            'empty_data' => function (FormInterface $form) {
                /** @var AbstractInterest $interest */
                $interest = $form->getConfig()->getOption('interest');
                /** @var AbstractQuestion&OptionsTrait $question */
                $question = $interest->getQuestion();

                $solution = $question->getMultiple() ? new MultiAnswerSolution() : new ChoiceSolution();
                $solution->setInterest($interest);

                return $solution;
            },
        ]);
        $resolver->setAllowedTypes('interest', AbstractInterest::class);
    }
}
