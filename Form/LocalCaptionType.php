<?php
/*
 * LocalCaptionType.php
 */

namespace AzureSpring\Bundle\SurveyBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormInterface;

class LocalCaptionType extends AbstractType
{
    /**
     * @param FormInterface|FormBuilderInterface $form
     * @param string                             $dataClass
     */
    public static function addLocalCaptions($form, $dataClass)
    {
        $form->add('localCaptions', MultilingualType::class, [
            'entry_type'   => LocalCaptionType::class,
            'provision'    => 'captions',
            'entry_options' => [
                'data_class' => $dataClass,
            ],
            'allow_add'    => true,
            'allow_delete' => true,
            'by_reference' => false,
        ]);
    }

    /**
     * @inheritDoc
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('locale')
            ->add('captions', CollectionType::class, [
                'entry_type'   => TextType::class,
                'allow_add'    => true,
                'allow_delete' => true,
            ])
        ;
    }
}
