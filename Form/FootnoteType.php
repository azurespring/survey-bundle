<?php
/*
 * FootnoteType.php
 */

namespace AzureSpring\Bundle\SurveyBundle\Form;

use AzureSpring\Bundle\SurveyBundle\Entity\Footnote;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * FootnoteType
 */
class FootnoteType extends AbstractType
{
    /**
     * @inheritDoc
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('paragraph')
            ->add('localParagraphs', MultilingualType::class, [
                'entry_type'   => LocalParagraphType::class,
                'provision'    => 'paragraph',
                'allow_add'    => true,
                'allow_delete' => true,
                'by_reference' => false,
            ])
        ;

        $builder->addEventListener(FormEvents::PRE_SUBMIT, function (FormEvent $event) {
            $data = $event->getData();

            if (!is_array($data)) {
                $event->setData(['paragraph' => $data]);
            }
        });
    }

    /**
     * @inheritDoc
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Footnote::class,
        ]);
    }
}
