<?php
/*
 * LocalParagraphType.php
 */

namespace AzureSpring\Bundle\SurveyBundle\Form;

use AzureSpring\Bundle\SurveyBundle\Entity\LocalParagraph;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class LocalParagraphType extends AbstractType
{
    /**
     * @inheritDoc
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('locale')
            ->add('paragraph')
        ;
    }

    /**
     * @inheritDoc
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => LocalParagraph::class,
        ]);
    }
}
