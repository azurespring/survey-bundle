<?php
/*
 * LocalLabelType.php
 */

namespace AzureSpring\Bundle\SurveyBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormInterface;

class LocalLabelType extends AbstractType
{
    /**
     * @param FormInterface|FormBuilderInterface $form
     * @param string                             $dataClass
     */
    public static function addLocalLabels($form, $dataClass)
    {
        $form->add('localLabels', MultilingualType::class, [
            'entry_type'   => LocalLabelType::class,
            'provision'    => 'label',
            'entry_options' => [
                'data_class' => $dataClass,
            ],
            'allow_add'    => true,
            'allow_delete' => true,
            'by_reference' => false,
        ]);
    }

    /**
     * @inheritDoc
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('locale')
            ->add('label')
        ;
    }
}
