<?php
/*
 * PredicateSolutionType.php
 */

namespace AzureSpring\Bundle\SurveyBundle\Form;

use AzureSpring\Bundle\SurveyBundle\Entity\AbstractInterest;
use AzureSpring\Bundle\SurveyBundle\Entity\PredicateSolution;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * PredicateSolutionType
 */
class PredicateSolutionType extends AbstractType
{
    /**
     * @inheritDoc
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('checked')
        ;
    }

    /**
     * @inheritDoc
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => PredicateSolution::class,
            'interest'   => null,
            'empty_data' => function (FormInterface $form) {
                $solution = new PredicateSolution();
                $solution->setInterest($form->getConfig()->getOption('interest'));

                return $solution;
            },
        ]);
        $resolver->setAllowedTypes('interest', AbstractInterest::class);
    }
}
