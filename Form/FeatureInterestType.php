<?php
/*
 * FeatureInterestType.php
 */

namespace AzureSpring\Bundle\SurveyBundle\Form;

use AzureSpring\Bundle\SurveyBundle\Entity\FeatureInterest;
use AzureSpring\Bundle\SurveyBundle\Entity\InterestLabel;
use AzureSpring\Bundle\SurveyBundle\Form\EventSubscriber\InterestSubscriber;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * FeatureInterestType
 */
class FeatureInterestType extends AbstractType
{
    /**
     * @inheritDoc
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        LocalLabelType::addLocalLabels($builder, InterestLabel::class);
        $builder
            ->add('permanentID')
            ->add('label')
            ->add('serialNumber')
        ;

        $builder->addEventSubscriber(new InterestSubscriber());
    }

    /**
     * @inheritDoc
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => FeatureInterest::class,
        ]);
    }
}
