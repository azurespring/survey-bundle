<?php
/*
 * MultilingualType.php
 */

namespace AzureSpring\Bundle\SurveyBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * MultilingualType
 */
class MultilingualType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->addEventListener(FormEvents::PRE_SUBMIT, function (FormEvent $event) use ($options) {
            if ($data = $event->getData()) {
                $event->setData(
                    array_map(
                        function ($v, $k) use ($options) {
                            if (is_int($k)) {
                                return $v;
                            }

                            $provision = $options['provision'];
                            if ('~' === $provision[0]) {
                                if (is_array($v)) {
                                    return $v + ['locale' => $k];
                                }

                                $provision = substr($provision, 1);
                            }

                            return [
                                $provision => $v,
                                'locale' => $k,
                            ];
                        },
                        $data,
                        array_keys($data)
                    )
                );
            }
        }, 10);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'provision' => null,
        ]);
        $resolver->setAllowedTypes('provision', 'string');
    }

    public function getParent()
    {
        return CollectionType::class;
    }
}
