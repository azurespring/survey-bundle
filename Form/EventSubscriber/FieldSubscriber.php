<?php
/*
 * FieldSubscriber.php
 */

namespace AzureSpring\Bundle\SurveyBundle\Form\EventSubscriber;

use AzureSpring\Bundle\SurveyBundle\Entity\AbstractField;
use AzureSpring\Bundle\SurveyBundle\Entity\AbstractLocalCaption;
use AzureSpring\Bundle\SurveyBundle\Entity\AbstractLocalLabel;
use AzureSpring\Bundle\SurveyBundle\Entity\ChoiceScalar;
use AzureSpring\Bundle\SurveyBundle\Entity\ChoiceVector;
use AzureSpring\Bundle\SurveyBundle\Entity\EssayScalar;
use AzureSpring\Bundle\SurveyBundle\Entity\FeatureInterest;
use AzureSpring\Bundle\SurveyBundle\Entity\Footnote;
use AzureSpring\Bundle\SurveyBundle\Entity\FreeInterest;
use AzureSpring\Bundle\SurveyBundle\Entity\OptionGroup;
use AzureSpring\Bundle\SurveyBundle\Entity\Predicate;
use AzureSpring\Bundle\SurveyBundle\Entity\RatingScalar;
use AzureSpring\Bundle\SurveyBundle\Entity\RatingVector;
use AzureSpring\Bundle\SurveyBundle\Entity\ScalarCaption;
use AzureSpring\Bundle\SurveyBundle\Entity\ScratchLabel;
use AzureSpring\Bundle\SurveyBundle\Entity\Section;
use AzureSpring\Bundle\SurveyBundle\Entity\SectionLabel;
use AzureSpring\Bundle\SurveyBundle\Entity\VectorCaption;
use AzureSpring\Bundle\SurveyBundle\Entity\VectorLabel;
use AzureSpring\Bundle\SurveyBundle\Form\FeatureInterestType;
use AzureSpring\Bundle\SurveyBundle\Form\FieldType;
use AzureSpring\Bundle\SurveyBundle\Form\FreeInterestType;
use AzureSpring\Bundle\SurveyBundle\Form\LocalCaptionType;
use AzureSpring\Bundle\SurveyBundle\Form\LocalLabelType;
use AzureSpring\Bundle\SurveyBundle\Form\OptionType;
use AzureSpring\Bundle\SurveyBundle\Form\ScratchLabelType;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;

/**
 * FieldSubscriber
 */
class FieldSubscriber implements EventSubscriberInterface
{
    /**
     * @var Section|ChoiceScalar|ChoiceVector|RatingScalar|RatingVector|EssayScalar|null
     */
    private $field;

    /**
     * @inheritDoc
     */
    public static function getSubscribedEvents()
    {
        return [
            FormEvents::PRE_SET_DATA => 'onPreSetData',
            FormEvents::PRE_SUBMIT   => 'onPreSubmit',
            FormEvents::SUBMIT       => 'onSubmit',
        ];
    }

    /**
     * @param FormEvent $event
     */
    public function onPreSetData(FormEvent $event)
    {
        $this->field = $event->getData();

        $event->setData($this);
    }

    /**
     * @param FormEvent $event
     */
    public function onPreSubmit(FormEvent $event)
    {
        $data = $event->getData();
        $form = $event->getForm();

        if (!array_key_exists('required', $data)) {
            $data['required'] = true;

            $event->setData($data);
        }

        if (array_key_exists('children', $data)) {
            $this->toggleField(Section::class);
            LocalLabelType::addLocalLabels($form, SectionLabel::class);
            $form
                ->add('label')
                ->add('children', CollectionType::class, [
                    'entry_type'   => FieldType::class,
                    'entry_options' => $form->getConfig()->getOption('allow_extra_fields') ? [
                        'allow_extra_fields' => true,
                    ] : [],
                    'allow_add'    => true,
                    'allow_delete' => true,
                    'by_reference' => false,
                ]);

            return;
        }

        if (!array_key_exists('interests', $data)) {
            [$choice, $rating, $caption] = [ChoiceScalar::class, RatingScalar::class, ScalarCaption::class];

            $build = function () use ($form) {
                $form->add('interest', FreeInterestType::class, $form->getConfig()->getOption('allow_extra_fields') ? [
                    'allow_extra_fields' => true,
                ] : []);
            };

            if (!array_key_exists('interest', $data)) {
                $data['interest'] = [
                    'label' => @$data['label'],
                    'localLabels' => @$data['localLabels'],
                ];

                unset($data['label']);
                unset($data['localLabels']);

                $event->setData($data);
            }
        } else {
            [$choice, $rating, $caption] = [ChoiceVector::class, RatingVector::class, VectorCaption::class];

            $build = function () use ($form) {
                LocalLabelType::addLocalLabels($form, VectorLabel::class);
                $form
                    ->add('label')
                    ->add('interests', CollectionType::class, [
                        'entry_type'    => FeatureInterestType::class,
                        'entry_options' => $form->getConfig()->getOption('allow_extra_fields') ? [
                            'allow_extra_fields' => true,
                        ] : [],
                        'allow_add'     => true,
                        'allow_delete'  => true,
                        'by_reference'  => false,
                    ]);
            };
        }

        if (array_key_exists('options', $data)) {
            $this->toggleField($choice);
            if (ChoiceScalar::class === $choice) {
                ScratchLabelType::addScratchLabels($form);
            }

            $form
                ->add('options', OptionType::class, $form->getConfig()->getOption('allow_extra_fields') ? [
                    'allow_extra_fields' => true,
                ] : [])
                ->add('multiple', null, [
                    'empty_data' => false,
                ])
                ->add('scratch')
                ->add('prompt')
            ;
        } elseif (array_key_exists('scale', $data)) {
            $this->toggleField($rating);
            LocalCaptionType::addLocalCaptions($form, $caption);
            $form
                ->add('scale')
                ->add('captions', CollectionType::class, [
                    'entry_type'   => TextType::class,
                    'allow_add'    => true,
                    'allow_delete' => true,
                ])
            ;
        } elseif (array_key_exists('predicate', $data)) {
            $this->toggleField(Predicate::class);
            $form
                ->add('predicate', TextType::class, ['mapped' => false])
            ;
        } else {
            $this->toggleField(EssayScalar::class);
        }

        $build();
    }

    /**
     * @param FormEvent $event
     */
    public function onSubmit(FormEvent $event)
    {
        $event->setData($this->field);
    }

    /**
     * @return string|null
     */
    public function getPermanentID(): ?string
    {
        return $this->field ? $this->field->getPermanentID() : null;
    }

    /**
     * @param string $permanentID
     *
     * @return $this
     */
    public function setPermanentID(string $permanentID): self
    {
        $this->field->setPermanentID($permanentID);

        return $this;
    }

    /**
     * @return int|null
     */
    public function getSerialNumber(): ?int
    {
        return $this->field ? $this->field->getSerialNumber() : null;
    }

    /**
     * @param int $serialNumber
     *
     * @return $this
     */
    public function setSerialNumber(int $serialNumber): self
    {
        $this->field->setSerialNumber($serialNumber);

        return $this;
    }

    /**
     * @return bool|null
     */
    public function isRequired(): ?bool
    {
        return $this->field ? $this->field->isRequired() : null;
    }

    /**
     * @param bool $required
     *
     * @return $this
     */
    public function setRequired(bool $required): self
    {
        $this->field->setRequired($required);

        return $this;
    }

    /**
     * @return Collection|AbstractField[]
     */
    public function getChildren(): Collection
    {
        return $this->field->getChildren();
    }

    /**
     * @param AbstractField $child
     *
     * @return $this
     */
    public function addChild(AbstractField $child): self
    {
        $this->field->addChild($child);

        return $this;
    }

    /**
     * @param AbstractField $child
     *
     * @return $this
     */
    public function removeChild(AbstractField $child): self
    {
        $this->field->removeChild($child);

        return $this;
    }

    /**
     * @return FreeInterest|null
     */
    public function getInterest(): ?FreeInterest
    {
        return $this->field->getInterest();
    }

    /**
     * @param FreeInterest $interest
     *
     * @return $this
     */
    public function setInterest(FreeInterest $interest): self
    {
        $this->field->setInterest($interest);

        return $this;
    }

    /**
     * @return string|null
     */
    public function getLabel(): ?string
    {
        return $this->field->getLabel();
    }

    /**
     * @param string $label
     *
     * @return $this
     */
    public function setLabel(string $label): self
    {
        $this->field->setLabel($label);

        return $this;
    }

    /**
     * @return Collection|AbstractLocalLabel[]
     */
    public function getLocalLabels(): Collection
    {
        return $this->field ? $this->field->getLocalLabels() : new ArrayCollection();
    }

    /**
     * @param AbstractLocalLabel $localLabel
     *
     * @return $this
     */
    public function addLocalLabel(AbstractLocalLabel $localLabel): self
    {
        $this->field->addLocalLabel($localLabel);

        return $this;
    }

    /**
     * @param AbstractLocalLabel $localLabel
     *
     * @return $this
     */
    public function removeLocalLabel(AbstractLocalLabel $localLabel): self
    {
        $this->field->removeLocalLabel($localLabel);

        return $this;
    }

    /**
     * @return Collection|FeatureInterest[]
     */
    public function getInterests(): Collection
    {
        return $this->field->getInterests();
    }

    /**
     * @param FeatureInterest $interest
     *
     * @return $this
     */
    public function addInterest(FeatureInterest $interest): self
    {
        $this->field->addInterest($interest);

        return $this;
    }

    /**
     * @param FeatureInterest $interest
     *
     * @return $this
     */
    public function removeInterest(FeatureInterest $interest): self
    {
        $this->field->removeInterest($interest);

        return $this;
    }

    /**
     * @return OptionGroup|null
     */
    public function getOptions(): ?OptionGroup
    {
        return $this->field->getOptions();
    }

    /**
     * @param OptionGroup $options
     *
     * @return $this
     */
    public function setOptions(OptionGroup $options): self
    {
        $this->field->setOptions($options);

        return $this;
    }

    /**
     * @return bool|null
     */
    public function getMultiple(): ?bool
    {
        return $this->field->getMultiple();
    }

    /**
     * @param bool $multiple
     *
     * @return $this
     */
    public function setMultiple(bool $multiple): self
    {
        $this->field->setMultiple($multiple);

        return $this;
    }

    /**
     * @return string|null
     */
    public function getScratch(): ?string
    {
        return $this->field->getScratch();
    }

    /**
     * @param string|null $scratch
     *
     * @return $this
     */
    public function setScratch(?string $scratch): self
    {
        $this->field->setScratch($scratch);

        return $this;
    }

    /**
     * @return string|null
     */
    public function getPrompt(): ?string
    {
        return $this->field->getPrompt();
    }

    /**
     * @param string|null $prompt
     *
     * @return $this
     */
    public function setPrompt(?string $prompt): self
    {
        $this->field->setPrompt($prompt);

        return $this;
    }

    /**
     * @return Collection|ScratchLabel[]
     */
    public function getLocalScratches(): Collection
    {
        return $this->field->getLocalScratches();
    }

    /**
     * @param ScratchLabel $localScratch
     *
     * @return $this
     */
    public function addLocalScratch(ScratchLabel $localScratch): self
    {
        $this->field->addLocalScratch($localScratch);

        return $this;
    }

    /**
     * @param ScratchLabel $localScratch
     *
     * @return $this
     */
    public function removeLocalScratch(ScratchLabel $localScratch): self
    {
        $this->field->removeLocalScratch($localScratch);

        return $this;
    }

    /**
     * @return int|null
     */
    public function getScale(): ?int
    {
        return $this->field->getScale();
    }

    /**
     * @param int $scale
     *
     * @return $this
     */
    public function setScale(int $scale): self
    {
        $this->field->setScale($scale);

        return $this;
    }

    /**
     * @return mixed
     */
    public function getCaptions()
    {
        return $this->field->getCaptions();
    }

    /**
     * @param mixed $captions
     *
     * @return $this
     */
    public function setCaptions($captions): self
    {
        $this->field->setCaptions($captions);

        return $this;
    }

    /**
     * @return Collection|AbstractLocalCaption[]
     */
    public function getLocalCaptions(): Collection
    {
        return $this->field ? $this->field->getLocalCaptions() : new ArrayCollection();
    }

    /**
     * @param AbstractLocalCaption $localCaption
     *
     * @return $this
     */
    public function addLocalCaption(AbstractLocalCaption $localCaption): self
    {
        $this->field->addLocalCaption($localCaption);

        return $this;
    }

    /**
     * @param AbstractLocalCaption $localCaption
     *
     * @return $this
     */
    public function removeLocalCaption(AbstractLocalCaption $localCaption): self
    {
        $this->field->removeLocalCaption($localCaption);

        return $this;
    }

    /**
     * @return Collection|Footnote[]
     */
    public function getFootnotes(): Collection
    {
        return $this->field ? $this->field->getFootnotes() : new ArrayCollection();
    }

    /**
     * @param Footnote $footnote
     *
     * @return $this
     */
    public function addFootnote(Footnote $footnote): self
    {
        $this->field->addFootnote($footnote);

        return $this;
    }

    /**
     * @param Footnote $footnote
     *
     * @return $this
     */
    public function removeFootnote(Footnote $footnote): self
    {
        $this->field->removeFootnote($footnote);

        return $this;
    }

    /**
     * @param string $class
     */
    private function toggleField(string $class)
    {
        if (!is_a($this->field, $class)) {
            $this->field = new $class();
        }
    }
}
