<?php
/*
 * OptionSubscriber.php
 */

namespace AzureSpring\Bundle\SurveyBundle\Form\EventSubscriber;

use AzureSpring\Bundle\SurveyBundle\Entity\AbstractOption;
use AzureSpring\Bundle\SurveyBundle\Entity\Option;
use AzureSpring\Bundle\SurveyBundle\Entity\OptionGroup;
use AzureSpring\Bundle\SurveyBundle\Entity\OptionLabel;
use AzureSpring\Bundle\SurveyBundle\Form\OptionType;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\PropertyAccess\PropertyAccess;

/**
 * OptionSubscriber
 */
class OptionSubscriber implements EventSubscriberInterface
{
    /**
     * @var Option|OptionGroup|null
     */
    private $option;

    /**
     * @inheritDoc
     */
    public static function getSubscribedEvents()
    {
        return [
            FormEvents::PRE_SET_DATA => 'onPreSetData',
            FormEvents::PRE_SUBMIT   => 'onPreSubmit',
            FormEvents::SUBMIT       => 'onSubmit',
        ];
    }

    /**
     * @param FormEvent $event
     */
    public function onPreSetData(FormEvent $event)
    {
        $this->option = $event->getData();

        $event->setData($this);
    }

    /**
     * @param FormEvent $event
     */
    public function onPreSubmit(FormEvent $event)
    {
        $form = $event->getForm();
        $data = $event->getData();

        if (!is_array($data)) {
            $event->setData($data = ['label' => $data]);
        } elseif (array_reduce(
            array_keys($data),
            function ($carry, $key) {
                return $carry && is_numeric($key);
            },
            true
        )) {
            $event->setData($data = ['children' => $data]);
        }

        if (!array_key_exists('children', $data)) {
            $this->toggleOption(Option::class);
        } else {
            $this->toggleOption(OptionGroup::class);

            $form->add('children', CollectionType::class, [
                'entry_type'    => OptionType::class,
                'entry_options' => $form->getConfig()->getOption('allow_extra_fields') ? [
                    'allow_extra_fields' => true,
                ] : [],
                'allow_add'     => true,
                'allow_delete'  => true,
                'by_reference'  => false,
            ]);
        }
    }

    /**
     * @param FormEvent $event
     */
    public function onSubmit(FormEvent $event)
    {
        $event->setData($this->option);
    }

    /**
     * @return string|null
     */
    public function getPermanentID(): ?string
    {
        return $this->option ? $this->option->getPermanentID() : null;
    }

    /**
     * @param string $permanentID
     *
     * @return $this
     */
    public function setPermanentID(string $permanentID): self
    {
        $this->option->setPermanentID($permanentID);

        return $this;
    }

    /**
     * @return int|null
     */
    public function getSerialNumber(): ?int
    {
        return $this->option ? $this->option->getSerialNumber() : null;
    }

    /**
     * @param int $serialNumber
     *
     * @return $this
     */
    public function setSerialNumber(int $serialNumber): self
    {
        $this->option->setSerialNumber($serialNumber);

        return $this;
    }

    /**
     * @return string|null
     */
    public function getLabel(): ?string
    {
        return $this->option ? $this->option->getLabel() : null;
    }

    /**
     * @param string $label
     *
     * @return $this
     */
    public function setLabel(string $label): self
    {
        $this->option->setLabel($label);

        return $this;
    }

    /**
     * @return Collection|OptionLabel[]
     */
    public function getLocalLabels(): Collection
    {
        return $this->option ? $this->option->getLocalLabels() : new ArrayCollection();
    }

    /**
     * @param OptionLabel $localLabel
     *
     * @return $this
     */
    public function addLocalLabel(OptionLabel $localLabel): self
    {
        $this->option->addLocalLabel($localLabel);

        return $this;
    }

    /**
     * @param OptionLabel $localLabel
     *
     * @return $this
     */
    public function removeLocalLabel(OptionLabel $localLabel): self
    {
        $this->option->removeLocalLabel($localLabel);

        return $this;
    }

    /**
     * @return Collection|AbstractOption[]
     */
    public function getChildren(): Collection
    {
        return $this->option->getChildren();
    }

    /**
     * @param AbstractOption $child
     *
     * @return $this
     */
    public function addChild(AbstractOption $child): self
    {
        $this->option->addChild($child);

        return $this;
    }

    /**
     * @param AbstractOption $child
     *
     * @return $this
     */
    public function removeChild(AbstractOption $child): self
    {
        $this->option->removeChild($child);

        return $this;
    }

    /**
     * @param string $class
     */
    private function toggleOption(string $class)
    {
        if (!is_a($this->option, $class)) {
            /**
             * @var Option|OptionGroup $option
             */
            $option = new $class();
            if ($this->option) {
                $accessor = PropertyAccess::createPropertyAccessor();
                foreach (['permanentID', 'serialNumber', 'label'] as $p) {
                    $v = $accessor->getValue($option, $p);
                    if (null !== $v) {
                        $accessor->setValue($option, $p, $v);
                    }
                }
            }

            $this->option = $option;
        }
    }
}
