<?php
/*
 * ChoiceSolutionSubscriber.php
 */

namespace AzureSpring\Bundle\SurveyBundle\Form\EventSubscriber;

use AzureSpring\Bundle\SurveyBundle\Entity\AbstractInterest;
use AzureSpring\Bundle\SurveyBundle\Entity\AbstractOption;
use AzureSpring\Bundle\SurveyBundle\Entity\AbstractQuestion;
use AzureSpring\Bundle\SurveyBundle\Form\Model\ChoiceSolutionInterface;
use AzureSpring\Bundle\SurveyBundle\Entity\ChoiceSolution;
use AzureSpring\Bundle\SurveyBundle\Entity\EssaySolution;
use AzureSpring\Bundle\SurveyBundle\Entity\Traits\OptionsTrait;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;

/**
 * ChoiceSolutionSubscriber
 */
class ChoiceSolutionSubscriber implements EventSubscriberInterface, ChoiceSolutionInterface
{
    /**
     * @var ChoiceSolution|EssaySolution
     */
    private $solution;

    /**
     * @inheritDoc
     */
    public static function getSubscribedEvents()
    {
        return [
            FormEvents::PRE_SET_DATA => 'onPreSetData',
            FormEvents::PRE_SUBMIT   => 'onPreSubmit',
            FormEvents::SUBMIT       => 'onSubmit',
        ];
    }

    /**
     * @param FormEvent $event
     */
    public function onPreSetData(FormEvent $event)
    {
        $this->solution = $event->getData();

        $event->setData($this);
    }

    /**
     * @param FormEvent $event
     */
    public function onPreSubmit(FormEvent $event)
    {
        $form = $event->getForm();
        $data = $event->getData();

        /** @var AbstractInterest $interest */
        $interest = $form->getConfig()->getOption('interest');
        /** @var AbstractQuestion&OptionsTrait $question */
        $question = $interest->getQuestion();
        if (array_key_exists('paragraph', $data ?? [])) {
            $this->toggleSolution(EssaySolution::class);

            $form->add('paragraph');
        } else {
            $this->toggleSolution(ChoiceSolution::class);

            $form->add('option', EntityType::class, [
                'class'        => AbstractOption::class,
                'choices'      => $question->getOptions()->rollOut(),
                'choice_value' => 'permanentID',
            ]);
        }
    }

    /**
     * @param FormEvent $event
     */
    public function onSubmit(FormEvent $event)
    {
        $event->setData($this->solution->setInterest($event->getForm()->getConfig()->getOption('interest')));
    }

    /**
     * @return AbstractOption|null
     */
    public function getOption(): ?AbstractOption
    {
        return $this->solution->getOption();
    }

    /**
     * @param AbstractOption|null $option
     *
     * @return $this
     */
    public function setOption(?AbstractOption $option): self
    {
        $this->solution->setOption($option);

        return $this;
    }

    /**
     * @return string|null
     */
    public function getParagraph(): ?string
    {
        return $this->solution->getParagraph();
    }

    /**
     * @param string|null $paragraph
     *
     * @return $this
     */
    public function setParagraph(?string $paragraph): self
    {
        $this->solution->setParagraph($paragraph);

        return $this;
    }

    /**
     * @param string $class
     */
    private function toggleSolution(string $class)
    {
        if (!is_a($this->solution, $class)) {
            $this->solution = new $class();
        }
    }
}
