<?php
/*
 * LocalLabelType.php
 */

namespace AzureSpring\Bundle\SurveyBundle\Form;

use AzureSpring\Bundle\SurveyBundle\Entity\ScratchLabel;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ScratchLabelType extends AbstractType
{
    /**
     * @param FormInterface|FormBuilderInterface $form
     */
    public static function addScratchLabels($form)
    {
        $form->add('localScratches', MultilingualType::class, [
            'entry_type'   => ScratchLabelType::class,
            'provision'    => '~label',
            'allow_add'    => true,
            'allow_delete' => true,
            'by_reference' => false,
        ]);
    }

    /**
     * @inheritDoc
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('locale')
            ->add('label')
            ->add('prompt')
        ;
    }

    /**
     * @inheritDoc
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => ScratchLabel::class,
        ]);
    }
}
