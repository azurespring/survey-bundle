<?php
/*
 * AbstractCase.php
 */

namespace AzureSpring\Bundle\SurveyBundle\Model;

use AzureSpring\Bundle\SurveyBundle\Entity\AbstractSolution;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * AbstractCase
 */
abstract class AbstractCase
{
    /**
     * @ORM\OneToMany(targetEntity="AzureSpring\Bundle\SurveyBundle\Entity\AbstractSolution", mappedBy="case", orphanRemoval=true, cascade={"persist"}, indexBy="interest_id")
     *
     * @Assert\Valid()
     */
    protected $solutions;

    /**
     * Constructor.
     */
    public function __construct()
    {
        $this->solutions = new ArrayCollection();
    }

    /**
     * @return Collection|AbstractSolution[]
     */
    public function getSolutions(): Collection
    {
        return $this->solutions;
    }

    /**
     * @param AbstractSolution $solution
     *
     * @return $this
     */
    public function addSolution(AbstractSolution $solution): self
    {
        $this->solutions[$solution->getInterest()->getPermanentID()] = $solution;
        $solution->setCase($this);

        return $this;
    }

    /**
     * @param AbstractSolution $solution
     *
     * @return $this
     */
    public function removeSolution(AbstractSolution $solution): self
    {
        if ($this->solutions->remove($solution->getInterest()->getPermanentID())) {
            // set the owning side to null (unless already changed)
            if ($solution->getCase() === $this) {
                $solution->setCase(null);
            }
        }

        return $this;
    }
}
